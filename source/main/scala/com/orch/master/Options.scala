package com.orch.master

import scopt.OParser

object Options {

    case class Config(
        addr : String = "127.0.0.1",
        port : Int = 5100,
        config : String = "config.yaml"
    )


    val builder = OParser.builder[Config]

    val parser1 = {
        import builder._
        OParser.sequence(
            programName("shell"),
            head("scopt", "4.x"),
            // option -f, --foo
            opt[String]('a', "addr")
                .action((x, c) => c.copy(addr=x))
                .text("address of the master node"),

            opt[Int]('p', "port")
                .action ((x, c) => c.copy (port=x))
                .text ("local port"),

            opt[String]('c', "config")
                .action ((x, c) => c.copy (config=x))
                .text ("config file for daemons")
        )
    }

}
