package com.orch.db

import java.util.concurrent.TimeUnit
import scala.concurrent.Await
import scala.concurrent.duration.Duration

import org.mongodb.scala._

trait ImplicitObservable[C] {
  val observable: Observable[C]
  val converter: (C) => String

  def results(): Seq[C] = Await.result(observable.toFuture(), Duration(10, TimeUnit.SECONDS))

  def headResult() = Await.result(observable.head(), Duration(10, TimeUnit.SECONDS))

  def printResults(initial: String = ""): Unit = {
    if (initial.length > 0) print(initial)
    results().foreach(res => println(converter(res)))
  }

  def printHeadResult(initial: String = ""): Unit = println(s"${initial}${converter(headResult())}")

}

