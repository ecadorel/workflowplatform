package com.orch.db

class FastLocalCollection {

    var _execs : Map [String, Executable] = Map ()

    var _tasks : Map [Long, Task] = Map ()

    var _flavors : Map [Long, Flavor] = Map ()

    var _vms : Map [Long, VM] = Map ()

    var _files : Map [Long, File] = Map ()

    var _replicas : Map [Long, Replica] = Map ()

    var _ioFiles : Map [Long, IOFile] = Map ()

    def insertOrReplace (exec : Executable) : Unit = {
        this.synchronized {
            this._execs = this._execs + (exec.e_id -> exec)
        }
    }

    def findExecutable (e_id : String) : Executable = {
        this.synchronized {
            if (this._execs.contains (e_id))
                this._execs (e_id)
            else null
        }
    }

    def insertOrReplace (task : Task) : Unit = {
        this.synchronized {
            this._tasks = this._tasks + (task.t_id -> task)
        }
    }

    def findTask (t_id : Long) : Task = {
        this.synchronized {
            if (this._tasks.contains (t_id))
                this._tasks (t_id)
            else null
        }
    }

    def findTasks () : Seq [Task] = {
        this.synchronized {
            return this._tasks.values.to[Seq]
        }
    }

    def findTasksRunning () : Seq [Task] = {
        this.synchronized {
            return this._tasks.values.to[Seq].filter (x => x.state == TaskState.RUNNING)
        }
    }

    def removeTask (t_id : Long) : Unit = {
        this.synchronized {
            this._tasks = this._tasks.filterKeys (_ != t_id)
        }
    }

    def insertOrReplace (f : Flavor) : Unit = {
        this.synchronized {
            this._flavors = this._flavors + (f.f_id -> f)
        }
    }

    def findFlavor (f_id : Long) : Flavor = {
        this.synchronized {
            if (this._flavors.contains (f_id))
                this._flavors (f_id)
            else null
        }
    }

    def insertOrReplace (v : VM) : Unit = {
        this.synchronized {
            this._vms = this._vms + (v.v_id -> v)
        }
    }

    def findVM (v_id : Long) : VM = {
        this.synchronized {
            if (this._vms.contains (v_id))
                this._vms (v_id)
            else null
        }
    }

    def deleteVM (v_id : Long) : Unit = {
        this.synchronized {
            this._vms = this._vms.filterKeys (_ != v_id)
        }
    }

    def findVMS () : Seq [VM] = {
        this.synchronized {
            this._vms.values.to[Seq]
        }
    }

    def insertOrReplace (file : File) : Unit = {
        this.synchronized {
            this._files = this._files + (file.f_id -> file)
        }
    }

    def findFile (f_id : Long) : File = {
        this.synchronized {
            if (this._files.contains (f_id))
                this._files (f_id)
            else null
        }
    }

    def insertOrReplace (repl : Replica) : Unit = {
        this.synchronized {
            this._replicas = this._replicas + (repl.r_id -> repl)
        }
    }

    def findReplica (r_id : Long) : Replica = {
        this.synchronized {
            if (this._replicas.contains (r_id))
                this._replicas (r_id)
            else null
        }
    }

    def replicasSize () : Long = {
        this.synchronized {
            return this._replicas.size
        }
    }

    def findReplicas (f_id : Long) : Seq [Replica] = {
        this.synchronized {
            this._replicas.values.to [Seq].filter (_.f_id == f_id)
        }
    }

    def findReplicas () : Seq [Replica] = {
        this.synchronized {
            this._replicas.values.to [Seq]
        }
    }

    def findReplicasSending () : Seq [Replica] = {
        this.synchronized {
            this._replicas.values.to [Seq].filter (_.state == FileState.SENDING)
        }
    }

    def findOutReplica (f_id : Long) : Replica = {
        this.synchronized {
            val repl = this._replicas.values.to [Seq].filter (x => x.rtype == ReplicaType.OUTPUT && x.f_id == f_id)
            if (repl.length == 0)
                null
            else repl (0)
        }
    }

    def findAllReplicasForTask (n_id : String, t_id : Long) : Seq [Replica] = {
        this.synchronized {
            this._replicas.values.to [Seq].filter (x => x.n_id == n_id && x.t_id == t_id)
        }
    }

    def findReplicaForTask (f_id : Long, n_id : String, t_id : Long) : Replica = {
        this.synchronized {
            val repl = this._replicas.values.to [Seq].filter (x => x.f_id == f_id && x.n_id == n_id && x.t_id == t_id)
            if (repl.length == 0)
                null
            else repl (0)
        }
    }

    def removeInputReplicasForTask (t_id : Long) : Unit = {
        this.synchronized {
            val repl = this._replicas.values.to [Seq].filter (x => x.t_id == t_id && x.rtype == ReplicaType.INPUT)
            for (r <- repl) {
                this._replicas = this._replicas.- (r.r_id)
            }
        }
    }

    def insertOrReplace (io : IOFile) : Unit = {
        this.synchronized {
            this._ioFiles = this._ioFiles + (io.f_id -> io)
        }
    }

    def findIO (f_id : Long) : IOFile = {
        this.synchronized {
            if (this._ioFiles.contains (f_id))
                this._ioFiles (f_id)
            else null
        }
    }

}
