package com.orch.db

import org.mongodb.scala._
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.bson.codecs.configuration.CodecRegistries.{fromRegistries, fromProviders}
import org.mongodb.scala.{MongoClient, MongoCollection, MongoDatabase}

class Collection (name : String) {
    import com.orch.db._
    import com.orch.db.Helpers._

    private val mongoClient = MongoClient ()
    private var db : MongoDatabase = mongoClient.getDatabase (name);

    private var _tasks : MongoCollection[Task] = db.withCodecRegistry (fromRegistries(fromProviders(
        classOf[Task]
    ), DEFAULT_CODEC_REGISTRY )).getCollection ("tasks")

    private var _execs : MongoCollection[Executable] = db.withCodecRegistry (fromRegistries(fromProviders(
        classOf[Executable]
    ), DEFAULT_CODEC_REGISTRY )).getCollection ("execs")

    private var _vms : MongoCollection[VM] = db.withCodecRegistry (fromRegistries(fromProviders(
        classOf[VM]
    ), DEFAULT_CODEC_REGISTRY )).getCollection ("vms")

    private var _nodes : MongoCollection[Node] = db.withCodecRegistry (fromRegistries(fromProviders(
        classOf[Node]
    ), DEFAULT_CODEC_REGISTRY )).getCollection ("nodes")

    private var _files : MongoCollection[File] = db.withCodecRegistry (fromRegistries(fromProviders(
        classOf[File]
    ), DEFAULT_CODEC_REGISTRY )).getCollection ("files")

    private var _replicas : MongoCollection[Replica] = db.withCodecRegistry (fromRegistries(fromProviders(
        classOf[Replica]
    ), DEFAULT_CODEC_REGISTRY )).getCollection ("replicas")

    private var _flavors : MongoCollection[Flavor] = db.withCodecRegistry (fromRegistries(fromProviders(
        classOf[Flavor]
    ), DEFAULT_CODEC_REGISTRY )).getCollection ("flavor")

    private var _usedIps : MongoCollection [Ip] = db.withCodecRegistry (fromRegistries(fromProviders(
        classOf[Ip]
    ), DEFAULT_CODEC_REGISTRY )).getCollection ("usedIps")

    private var _clusters : MongoCollection [Cluster] = db.withCodecRegistry (fromRegistries(fromProviders(
        classOf[Cluster]
    ), DEFAULT_CODEC_REGISTRY )).getCollection ("clusters")

    private var _iofiles : MongoCollection [IOFile] = db.withCodecRegistry (fromRegistries(fromProviders(
        classOf[IOFile]
    ), DEFAULT_CODEC_REGISTRY )).getCollection ("iofiles")


    private var _sched : MongoCollection [SchedulerInfo] = db.withCodecRegistry (fromRegistries(fromProviders(
        classOf[SchedulerInfo]
    ), DEFAULT_CODEC_REGISTRY )).getCollection ("sched_info")

    var waiting : List [Observable[Completed]] = List ()

    def replicateFastCollection (fast : FastLocalCollection) : Unit = {
        for (e <- fast._execs) this.insertOrReplace (e._2)
        println ("execs")
        for (t <- fast._tasks) this.insertOrReplace (t._2)
        println ("tasks")
        for (f <- fast._flavors) this.insertOrReplace (f._2)
        println ("flvs")
        for (v <- fast._vms) this.insertOrReplace (v._2)
        println ("vms")
        // for (f <- fast._files) this.insertOrReplace (f._2)
        // println ("files")
        // for (r <- fast._replicas) this.insertOrReplace (r._2)
        // println ("repls")
        // for (i <- fast._ioFiles) this.insertOrReplace (i._2)
        // println ("ios")
    }

    def insertOrReplace (task : Task) : Unit = {
        this._tasks.find (equal ("t_id", task.t_id)).results.foreach (
            (doc : Task) =>
            this._tasks.deleteOne (equal ("t_id", task.t_id)).results ()
        )

        this._tasks.insertOne (task).results ()        
    }

    def findTask (t_id : Long) : Task = {
        this.await ()
        val tasks = this._tasks.find (equal ("t_id", t_id)).results ()
        return if (tasks.length == 0)
            null
        else tasks.head
    }

    def findTasks () : Seq[Task] = {
        this.await ()
        this._tasks.find ().results ()
    }

    def findTasksRunning () : Seq[Task] = {
        this.await ()
        this._tasks.find (equal ("state", TaskState.RUNNING)).results ()
    }

    def removeTask (t_id : Long) : Unit = {
        this._tasks.deleteOne (equal ("t_id", t_id)).results ()
    }

    def insertOrReplace (exec : Executable) : Unit = {
        this._execs.find (equal ("e_id", exec.e_id)).results.foreach (
            (doc : Executable) => this._execs.deleteOne (equal ("e_id", exec.e_id)).results ()
        )

        this._execs.insertOne (exec).results ()        
    }

    def findExecutable (e_id : String) : Executable = {
        this.await ()
        val execs = this._execs.find (equal ("e_id", e_id)).results ()
        return if (execs.length == 0)
            null
        else execs.head
    }

    def findExecutables () : Seq[Executable] = {
        this.await ()
        this._execs.find ().results ()
    }

    def insertOrReplace (v : VM) : Unit = {
        this._vms.find (equal ("v_id", v.v_id)).results.foreach (
            (doc: VM) => this._vms.deleteOne (equal ("v_id", v.v_id)).results ()
        )

        this._vms.insertOne (v).results ()
    }

    def findVM (v_id : Long) : VM = {
        this.await ()
        val vms = this._vms.find (equal ("v_id", v_id)).results ()
        return if (vms.length == 0)
            null
        else vms.head
    }

    def deleteVM (v_id : Long) : Unit = {
        this.await ()
        this._vms.find (equal ("v_id", v_id)).results ().foreach (
            (doc : VM) => this._vms.deleteOne (equal ("v_id", v_id)).results ()
        )
    }

    def findVMs () : Seq[VM] = {
        this.await ()
        this._vms.find ().results ()
    }

    def insertOrReplace (node : Node) : Unit = {
        this._nodes.find (equal ("n_id", node.n_id)).results.foreach (
            (doc : Node) => this._nodes.deleteOne (equal ("n_id", node.n_id)).results ()
        )

        this._nodes.insertOne (node).results ()        
    }

    def findNode (n_id : String) : Node = {
        this.await ()
        val nodes = this._nodes.find (equal ("n_id", n_id)).results ()
        return if (nodes.length == 0)
            null
        else nodes.head
    }

    def findNodes () : Seq[Node] = {
        this.await ()
        this._nodes.find ().results ()
    }

    def insertOrReplace (cluster : Cluster) : Unit = {
        this._clusters.find (equal ("name", cluster.name)).results.foreach (
            (doc : Cluster) => this._clusters.deleteOne (equal ("name", cluster.name)).results ()
        )

        this._clusters.insertOne (cluster).results ()
    }

    def findCluster (name : String) : Cluster = {
        this.await ()
        val clusters = this._clusters.find (equal ("name", name)).results ()
        return if (clusters.length == 0)
            null
        else clusters.head
    }

    def findClusters () : Seq [Cluster] = {
        this.await ()
        this._clusters.find ().results ()
    }

    def insertOrReplace (file : File) : Unit = {
        this._files.find (equal ("f_id", file.f_id)).results.foreach (
            (doc : File) => this._files.deleteOne (equal ("f_id", file.f_id)).results ()
        )

        this._files.insertOne (file).results ()        
    }

    def findFile (f_id : Long) : File = {
        this.await ()
        val files = this._files.find (equal ("f_id", f_id)).results ()
        return if (files.length == 0)
            null
        else return files.head
    }

    def findFiles () : Seq[File] = {
        this.await ()
        this._files.find ().results ()
    }

    def insertOrReplace (repl : Replica) : Unit = {
        this._replicas.find (equal ("r_id", repl.r_id)).results.foreach (
            (doc : Replica) => this._replicas.deleteOne (equal ("r_id", repl.r_id)).results ()
        )

        this._replicas.insertOne (repl).results ()
    }

    def findReplica (r_id : Long) : Replica = {
        this.await ()
        val repls = this._replicas.find (equal ("r_id", r_id)).results ()
        return if (repls.length == 0)
            null
        else return repls.head
    }

    def findReplicas (f_id : Long) : Seq [Replica] = {
        this.await ()
        this._replicas.find (equal ("f_id", f_id)).results ()
    }

    def findReplicas () : Seq [Replica] = {
        this.await ()
        this._replicas.find ().results ()
    }   

    def findReplicasSending () : Seq [Replica] = {
        this.await ()
        this._replicas.find (equal("state", FileState.SENDING)).results ()
    }

    def findOutReplica (f_id : Long) : Replica = {
        this.await ()
        val repls = this._replicas.find (and (equal ("f_id", f_id), equal ("rtype", ReplicaType.OUTPUT))).results ()
        return if (repls.length == 0)
            null
        else return repls.head
    }

    def findReplicaForTask (f_id : Long, n_id : String, t_id : Long) : Replica = {
        this.await ()
        val repls = this._replicas.find (and (and (equal ("f_id", f_id), equal ("n_id", n_id)), equal ("t_id", t_id))).results ()
        return if (repls.length == 0)
            null
        else return repls.head
    }

    def findAllReplicasForTask (n_id : String, t_id : Long) : Seq [Replica] = {
        this.await ()
        this._replicas.find (and (equal ("n_id", n_id), equal ("t_id", t_id))).results ()        
    }

    def removeReplicaForTask (t_id :Long) = {
    }

    def insertOrReplace (flav : Flavor) : Unit = {
        this._flavors.find (equal ("f_id", flav.f_id)).results.foreach (
            (doc : Flavor) => this._flavors.deleteOne (equal ("f_id", flav.f_id)).results ()
        )

        this._flavors.insertOne (flav).results ()
    }

    def findFlavor (f_id : Long) : Flavor = {
        this.await ()
        val flavs = this._flavors.find (equal ("f_id", f_id)).results ()
        return if (flavs.length == 0)
            null
        else flavs.head
    }

    def findFlavors () : Seq[Flavor] = {
        this.await ()
        this._flavors.find ().results ()
    }

    def insertIp (ip : Ip) : Unit = {
        this._usedIps.find (equal ("v_id", ip.v_id)).results.foreach (
            (i : Ip) => this._usedIps.deleteOne (equal ("v_id", i.v_id)).results ()
        )

        this._usedIps.find (equal ("ip", ip.ip)).results.foreach (
            (i : Ip) => this._usedIps.deleteOne (equal ("v_id", i.v_id)).results ()
        )

        this._usedIps.insertOne (ip).results ()
    }

    /**
      * Supprime l'ip utilise par la vm v_id (elle ne l'utilise plus)
      */
    def deleteIp (v_id : Long) : Unit = {
        this.await ()
        this._usedIps.find (equal ("v_id", v_id)).results.foreach (
            (i : Ip) => this._usedIps.deleteOne (equal ("v_id", v_id)).results ()
        )
    }

    def findIp (v_id : Long) : Ip = {
        this.await ()
        val ips = this._usedIps.find (equal ("v_id", v_id)).results
        return if (ips.length == 0)
            null
        else return ips.head
    }

    def getUsedIp (): Seq[Ip] = {
        this.await ()
        this._usedIps.find ().results ()
    }


    def insertOrReplace (io : IOFile) : Unit = {
        this._iofiles.find (equal ("f_id", io.f_id)).results.foreach (
            (doc : IOFile) => this._iofiles.deleteOne (equal ("f_id", io.f_id)).results ()
        )

        this._iofiles.insertOne (io).results ()        
    }

    def findIOs () : Seq [IOFile] = {
        this.await ()
        this._iofiles.find ().results ()
    }

    def findIO (id : Long) : IOFile = {
        this.await ()
        val file = this._iofiles.find (equal ("f_id", id)).results ()
        return if (file.length == 0)
            null
        else file.head
    }

    def insertOrReplace (sched : SchedulerInfo) : Unit = {
        this._sched.insertOne (sched).results ()
    }

    def await () {
        // this.waiting.synchronized {
        //     waiting.foreach {
        //         x => x.results ()
        //     }
        //     waiting = List ()
        //}
    }


}
