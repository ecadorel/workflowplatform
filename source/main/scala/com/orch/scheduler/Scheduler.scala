package com.orch.scheduler
import java.io.{File}
import com.orch.db._
import com.orch.utils._
import akka.actor.{Props, Actor, ActorSystem, ActorRef, ActorLogging, ActorSelection, PoisonPill }
import java.net.InetSocketAddress
import util.control.Breaks._
import scala.math.{max, min, ceil}
import java.util.concurrent.TimeUnit
import scala.concurrent.Await
import scala.concurrent.Future
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits.global

object SchedType {
    val BEST_EFFORT = 0
    val ON_TIME     = 1
}

abstract class Scheduler (parent : ScheduleActor, ref : ActorSelection, RAddr : String, RPort : Int, schedType : Long = SchedType.BEST_EFFORT, byTask : Boolean = true)  // l'actor ref, pour pouvoir communiquer les ordres
{
    import com.orch.db._
    import com.orch.master.MonitorProto._
    import com.orch.daemon.DaemonProto._
    import com.orch.file._

    var startSchedule = System.currentTimeMillis () / 1000
    var lastSchedule : Long = -1
    val SPEED_DEGRAD = 0.95
    var fut : Future[Unit] = null
    var failed : Map [Long, Int] = Map ()

    case class Infrastructure (
        var nodes : Map [String, SchedNode],
        var bw : Map [String, Map [String, Int]]
    )

    var coll = new FastLocalCollection ()
    var load : List [Workflow] = List ()    
    var custom_os : Map [String, (String, String, Int, Int)] = Map ()
    var infra : Infrastructure = initCurrentLoad ()

    var launched : List[Long] = List ()
    var killed : List [Long] = List ()
    var error : List [Long] = List ()

    def addCustomOs (os : Map [String, (String, String, Int, Int)]) : Unit = {
        custom_os = custom_os ++ os
        os foreach { x => println (x._1, "->", x._2) }
    }

    def addLoad (load : List [Workflow]) : Unit = {
        println ("New load : " + load(0).start + " " + load (0).deadline)
        for (w <- load) {
            var dones : Set [String] = Set ()
            val begin = System.currentTimeMillis ()
            for (t <- w.tasks) {
                var src_path = t._2.work.app
                var e_id = new java.io.File (src_path).getName ()
                var e_path = new java.io.File (src_path).getAbsolutePath ()
                var path = "exec:" + e_id
                if (!dones.contains (path)) {
                    ref ! MonitorProto.UploadExec (e_id, e_path)
                    dones += path
                }
            }
            val inter = System.currentTimeMillis ()
            println ("Tasks : " + (inter - begin) + "ms")
            for (f <- w.files) {
                //this.coll.insertOrReplace (com.orch.db.File (f._1, f._2.name, w.user, f._2.size))
                ref ! MonitorProto.AddFile (f._1, f._2.name, w.user, f._2.size)

                if (f._2.io == SchedFileType.INPUT) {
                    //println (s"Register input : ${f._1}")
                    var src_path = Path.build (Path.build (Path (w.path), "input"), f._2.name).file
                    var abs = new java.io.File (src_path).getAbsolutePath ()
                    ref ! MonitorProto.RegisterInput (f._1, abs)
                }
            }
            val end = System.currentTimeMillis ()
            println ("Files : " + (end - inter) + "ms")
        }
        val begin = System.currentTimeMillis ()
        onNewLoad (load)
        val end = System.currentTimeMillis ()
        println ("Final " + (end - begin) + "ms")
        println (currentTime ())
    }

    def onNewLoad (load : List [Workflow]) : Unit

    def scheduleAndRun (mutex : Integer) : Unit = {
        for (f <- this.load) if (f.isFinished)
            onFinishFlow (f.id, f)

        var list : Seq [SchedTask] = Seq ()
        if (this.byTask) {
            for (f <- load) {
                list ++= f.getAllSchedubale ()
            }

            list = scheduleTasks (list, mutex)
        } else {
            var listFlow : Seq [Workflow] = Seq ()
            for (f <- load) {
                if (isSchedulable (f))
                    listFlow = listFlow :+ f
            }

            list = scheduleFlows (listFlow, mutex)
        }

        mutex.synchronized {
            run ()// On run, peut être que des taches sont lançable, le schedule a pu prendre du temps
        }

        // if (list.length != 0) {
        //     fut = Future {
        //         for (t <- list.sortWith (_.loc.start < _.loc.start)) {
        //             if (t.loc != null)
        //                 //mutex.synchronized {)
        //                 copyAllReplicate (t) // ça aussi c'est long
        //                 //}
        //         }
        //     }
        // }

        // mutex.synchronized {
        //     run () // On rerun, le schedule a pu changer des choses
        // }
    }

    def delayed () {
    }

    def findCustomOs (os : String) : (String, String, Int, Int) = {
        if (custom_os.contains (os))
            custom_os (os)
        else (os, "", 60 * 2000, 1)
    }

    def computeBoot (os : String) : Long = {
        val customOs = findCustomOs (os)
        return NormalDistribution (customOs._3, customOs._4).inverseCDF (Utils.PROBA_VM).toLong
    }

    def onTaskFinished (id : Long, mutex : Integer) : Unit = {
        for (f <- load) {
            val res = f.getTask (id)
            res match {
                case Some (t) =>
                    val vm = t.loc.vm
                    t.loc.release ()
                    t.setFinished ()
                    var repls : Map [Long, Long] = Map ()
                    for (x <- t.work.succ) {
                        val replId = if (repls.contains (x.file.id)) {
                            repls (x.file.id)
                        } else {  // On enregistre le replica cree
                            val _id = IdGenerator.nextId ()
                            coll.insertOrReplace (Replica (_id, x.file.id, t.loc.vm.node.name, t.id, ReplicaType.OUTPUT, FileState.SENT, System.currentTimeMillis (), System.currentTimeMillis ()))
                            ref ! MonitorProto.ReplicaReg (_id, x.file.id, t.loc.vm.node.name, t.id)
                            repls += (x.file.id -> _id)
                            _id
                        }

                        if (x.file.io == SchedFileType.OUTPUT) { // Si c'est un output on telecharge
                            val sec_id = IdGenerator.nextId ()
                            val path = new java.io.File (Path.build (Path.build (Path (f.path), "output"), x.file.name).file).getAbsolutePath ()
                            this.coll.insertOrReplace (Replica (sec_id, x.file.id, "output", -1, ReplicaType.RESULT, FileState.SENDING, System.currentTimeMillis (), 0))
                            ref ! MonitorProto.DownloadOuput (replId, sec_id, path)
                        }

                        if (x.task != null && x.task.loc != null) { // si il y a un heriter schedulé on envoi
                            val sec_id = IdGenerator.nextId ()
                            this.coll.insertOrReplace (Replica (sec_id, x.file.id, x.task.loc.vm.node.name, x.task.id, ReplicaType.INPUT, FileState.SENDING, System.currentTimeMillis (), 0))
                            ref! MonitorProto.SendFile (replId, sec_id, x.task.loc.vm.node.name, x.task.id);
                        }
                    }

                    this.coll.removeInputReplicasForTask (t.id)
                    ref ! MonitorProto.RmTaskInputs (t.id)                    
                case _ => {}
            }
        }
    }

    def onFinishFlow (id : Long, f : Workflow) : Unit = {
        this.load = this.load.filter (_.id != f.id)
        println (s"[1;32m Flow ${f.id} for user ${f.user} has finished[0m")
        println (s"Rest ${this.load.size} flows")
    }

    def isFinished () : Boolean = {
        this.load.size == 0
    }

    /**
      *  Si on est ici, probablement qu'un input n'est pas bon
      */
    def onTaskError (id : Long) : Unit = {
        println (s"Task : $id failed")
        for (f <- load) {
            val res = f.getTask (id)
            res match {
                case Some (t) =>
                    val e_id = new java.io.File (t.work.app).getName()
                    t.setNone ()
                    copyAllReplicateForce (t)                    
                    return
                case _ => {}
            }
        }
    }

    def onVmReady (id : Long) : Unit = {
        println (s"VM is ready : $id")
        for (n <- infra.nodes) {
            val res = n._2.getVM (id)
            res match {
                case Some (v) =>
                    if (v.getState != VMState.KILLING) { // Elle etait en train de boot, coincidence regrettable, on l'eteint pile au moment ou elle est prete
                        v.setRunning ()
                        runOnVm (v)
                    }
                    return
                case _ => {} // Sinon le compilo est pas content...
            }
        }
    }

    def onFileSent (id : Long) : Unit = {
        val repl = coll.findReplica (id)
        coll.insertOrReplace (Replica (id, repl.f_id, repl.n_id, repl.t_id, repl.rtype, FileState.SENT, repl.creation, repl.available))
    }

    def onFileFailure (id : Long) : Unit = {
        println ("[1;31mFile failure !![0m")
        val repl = coll.findReplica (id)
        coll.insertOrReplace (Replica (id, repl.f_id, repl.n_id, repl.t_id, repl.rtype, FileState.FAILURE, repl.creation, repl.available))

        for (f <- load) {
            val res = f.getTask (repl.t_id)
            res match {
                case Some (t) =>
                    copyAllReplicate (t)
                    return
                case _ => {}
            }
        }
    }

    def onVmOff (id : Long) : Unit = {
        println (s"[1;33mOK vm : ${id} is off[0m")
        for (node <- infra.nodes) {
            val res = node._2.getVM (id)
            res match {
                case Some (v) =>
                    println (s"[1;33mKill VM : ${v}[0m")
                    if (v.getTasks.size != 0) {
                        println (s"[1;33mTime out vm off ${v}[0m")
                        v.setOff ()
                        v.changeId ()
                    } else
                        v.setKill ()
                    return
                case _ => {}
            }
        }
    }

    def onVmError (id : Long) : Unit = {
        println (s"Arghh, vm ${id} failed")
        for (node <- infra.nodes) {
            val res = node._2.getVM (id)
            res match {
                case Some (v) =>
		    if (v.getState () == VMState.BOOTING) {
                        println (s"OK relauch vm : ${id}")
		        launchVM (v)
                    } else if (v.getState () == VMState.PAUSE) {
                        println (s"OK relauch vm : ${id}")
		        launchVM (v)
                    }
                    return
                case _ => {}
            }
        }
    }

    def initCurrentLoad () : Infrastructure = {
        val coll = new Collection ("orch")
        val daemons = coll.findNodes ()
        val clusters = coll.findClusters ()

        var nodes : Map [String, SchedNode] = Map ()
        var bw : Map [String, Map [String, Int]] = Map ()

        daemons foreach {
            d => {
                nodes += (d.n_id -> new SchedNode (d.n_id, d.cluster, d.speed, d.capas))
            }
        }

        clusters foreach {
            c => { bw += (c.name -> c.bw) }
        }

        println ("FIN")
        Infrastructure (nodes, bw)        
    }

    def launchVM (v : SchedVM) : Unit = {
        println (s"Launch vm : ${v.id}")
        v.setLaunch (currentTime ())
        val (os, script, _, _) = findCustomOs (v.os)
        val id = IdGenerator.nextId ()
        ref ! MonitorProto.AddFlavor (id, os, v.capas, script)
        ref ! MonitorProto.LaunchVM (v.id, id, v.user, v.node.name)        
    }

    def execTask (t : SchedTask) : Unit = {
        println ("Run task : " + t.id + " " + t.loc.vm.node.name + " " + t.work.user)
        t.setRunning ()
        val e_id = new java.io.File (t.work.app).getName ()
        ref ! MonitorProto.RunTask (t.id, e_id, t.loc.vm.id, t.work.params)
    }

    def killVM (v : SchedVM) : Unit = {
        v.setKilling ()
        ref ! MonitorProto.KillVM (v.id)
        println (s"[1;32mKill VM ${v.id}[0m")
    }

    def pauseVM (v : SchedVM) : Unit = {
        v.setPause ()
        ref ! MonitorProto.PauseVM (v.id)
    }

    def resumeVM (v : SchedVM) : Unit = {
        v.setResume ()
        ref ! MonitorProto.ResumeVM (v.id)
    }

    /**
      * A priori, les decisions prise sont independante de la façon dont on les prend
      * Donc, on doit pouvoir appliquer une méthode générique de changement d'état
      */
    def run (fast : Boolean = false) : Unit = {
        var list : Seq [Future[Unit]] = Seq ()
        for (n <- infra.nodes) {
            val th : Future[Unit] = Future {
                    runOnNode (n._2, fast)
                }
            list = list :+ th
        }

        for (t <- list)            
            Await.result(t, Duration.Inf)            
    }

    def runOnNode (node : SchedNode, fast : Boolean = false) : Unit = {
        println ("Run on node : " + node.name)
        var currentUse : Map [String, Int] = Map ()
        val vms = node.getVMs ().values.to[Seq].sortBy (r => (r.start, r.end))

        var nb_cpus = 0
        for (v <- vms) {
            var killed : Boolean = false
            if (!fast) {
                if ((v.start == v.end && v.end == 0) && (v.getState () == VMState.RUNNING || v.getState () == VMState.BOOTING)) {
                    killVM (v)
                    killed = true
                }
                else if (v.getTasks ().size == 0 && (v.getState () == VMState.RUNNING || v.getState () == VMState.BOOTING)) {

                    killVM (v)
                    killed = true
                }
            }

            if (!killed) {
                nb_cpus += v.getCapas ()("cpus")
                if (nb_cpus > node.getCapas ()("cpus"))
                    return

                runOnVm (v, fast)                
            }
        }
    }

    def runOnVm (vm : SchedVM, fast : Boolean = false) : Unit = {
        if (isLaunchable (vm)) {
            launchVM (vm)
            fut = Future {
                for (t <- vm.getTasks.values.to[Seq].sortWith (_.loc.start < _.loc.start))
                    copyAllInput (t)
            }
        } else if (vm.getState () == VMState.RUNNING) {
            val tasks = vm.getTasks.values.to [Seq].sortWith (_.loc.start < _.loc.start)
            if (tasks.length == 0) killVM (vm)

            var nb_cpus = 0
            for (t <- tasks) {
                nb_cpus += 1
                if (nb_cpus > vm.getCapas ()("cpus")) return
                if (// t.loc.start <= this.currentTime && 
                    t.loc.state != TaskState.RUNNING) {
                    runOnTask (t, fast)
                }
            }
        } 
    }

    def runOnTask (t : SchedTask, fast : Boolean = false) : Unit = {
        if (hasAllReplicate (t)) {
            if (isLaunchable (t)) {
                execTask (t)
            }
        } else {
            copyAllReplicate (t)
        }
    }

    def isLaunchable (vm : SchedVM) : Boolean = {
        if (this.schedType == SchedType.BEST_EFFORT) {
            if (vm.getState () == VMState.DOWN) return true
            else return false
        } else if (this.schedType == SchedType.ON_TIME) {
            if (vm.getState () == VMState.DOWN && (vm.isBestEffort () || vm.getStart () - vm.getBootTime () <= this.currentTime ()))
                return true
            else return false
        }
        false
    }

    /** 
      * Assume task has already all its needed replicate      
      */
    def isLaunchable (task : SchedTask) : Boolean = {
        //if (this.schedType == SchedType.BEST_EFFORT) {
        if (task.loc.state != TaskState.RUNNING) return true
        else return false
        //} else if (this.schedType == SchedType.ON_TIME) {
        //     if (task.loc.state != TaskState.RUNNING && (task.meta.isBestEffort () || task.loc.start <= this.currentTime ()))
        //         return true
        //     else return false
        // }
        // false
    }

    def copyReplicateOnThreeLevel (vm : SchedVM) : Unit = {
        val tasks = vm.getTasks.values.to [Seq].sortWith (_.loc.start < _.loc.start)
        var capas = vm.getCapas ()
        for (k <- capas) {
            capas = capas + (k._1 -> k._2 * 3)
        }

        var currentUse : Map [String, Int] = Map ()
        for (t <- tasks) {
            val (poss, use) = computeMultiCapa (t.work.needs, currentUse, capas)
            if (poss) {
                copyAllReplicate (t)
                currentUse = use
            }
        }
    }

    def computeMultiCapa (add : Map [String, Int], pred : Map [String, Int], caps : Map [String, Int]) : (Boolean, Map [String, Int]) = {
        var res : Map [String, Int] = Map ()
        var valid = true
        for (capa <- caps) {
            if (pred.contains (capa._1)) {
                if (add.contains (capa._1)) {
                    res += (capa._1 -> (pred (capa._1) + add (capa._1)))
                    if (res (capa._1) > capa._2)
                        valid = false
                } else res += (capa._1 -> pred (capa._1))
            } else if (add.contains (capa._1)) {
                res += (capa._1 -> add (capa._1))
                if (res (capa._1) > capa._2) {
                    println ("[1;31m There's no way this (vm or task) can be launched[0m")
                    System.exit (-1)
                }
            }
        }
        return (valid, res)
    }

    def scheduleTasks (tasks : Seq[SchedTask], mutex : Integer) : Seq[SchedTask]

    def scheduleFlows (flow : Seq [Workflow], mutex : Integer) : Seq [SchedTask] = {
        Seq ()
    }

    def isSchedulable (flow : Workflow) : Boolean = {
        false
    }

    def replicasForTasks (tasks_ : Seq [SchedTask]) : Unit = {
        var used : Map [String, Seq[Interval]] = Map ()
        val tasks = tasks_.sortWith (_.loc.start < _.loc.start)
        for (t <- tasks) {
            if (t.loc.state == TaskState.NONE)
                copyAllReplicate (t)
        }
    }

    def findSentReplica (t_id : Long, n_id : String, f : Long) : Boolean = {
        val repl = coll.findReplicaForTask (f, n_id, t_id)
        if (repl == null || repl.state != FileState.SENT) {
            false
        } else true
    }

    def findReplica (t_id : Long, n_id : String, f : Long) : Replica = {
        coll.findReplicaForTask (f, n_id, t_id)
    }

    def hasAllReplicate (t : SchedTask) : Boolean = {
        val begin = System.currentTimeMillis ()
        val repls = coll.findAllReplicasForTask (t.loc.vm.node.name, t.id)
        for (edge <- t.work.pred) {
            var canBeTrue : Boolean = false
            breakable {
                for (repl <- repls) {
                    if (repl.f_id == edge.file.id && repl.state == FileState.SENT) {
                        canBeTrue = true
                        break
                    }
                }
            }
            if (!canBeTrue) {
                val end = System.currentTimeMillis ()
                return false
            }
        }
        val end = System.currentTimeMillis ()
        return true
    }

    def copyAllReplicate (t : SchedTask) : Unit = {
        if (t.loc == null) return
        for (edge <- t.work.pred) {
            val sendingReplica = findReplica (t.id, t.loc.vm.node.name, edge.file.id)
            if (sendingReplica == null) {
                val outRepl = coll.findOutReplica (edge.file.id)
                if (outRepl != null) {
                    val id = IdGenerator.nextId ()
                    this.coll.insertOrReplace (Replica (id, outRepl.f_id, t.loc.vm.node.name, t.id, ReplicaType.INPUT, FileState.SENDING, System.currentTimeMillis (), 0))
                    ref ! MonitorProto.SendFile (outRepl.r_id, id, t.loc.vm.node.name, t.id)
                } else if (edge.task == null) { // Input file
                    val id = IdGenerator.nextId ()
                    this.coll.insertOrReplace (Replica (id, edge.file.id, t.loc.vm.node.name, t.id, ReplicaType.INPUT, FileState.SENDING, System.currentTimeMillis (), 0))
                    ref ! MonitorProto.SendInput (edge.file.id, id, t.loc.vm.node.name, t.id)
                }
            }
        }
    }

    def copyAllInput (t : SchedTask) : Unit = {
        if (t.loc == null) return
        for (edge <- t.work.pred) {
            if (edge.task == null) { // Input file
                val id = IdGenerator.nextId ()
                this.coll.insertOrReplace (Replica (id, edge.file.id, t.loc.vm.node.name, t.id, ReplicaType.INPUT, FileState.SENDING, System.currentTimeMillis (), 0))
                ref ! MonitorProto.SendInput (edge.file.id, id, t.loc.vm.node.name, t.id)
            }
        }
    }

    def copyAllReplicateForce (t : SchedTask) : Unit = {
        if (t.loc == null) return
        for (edge <- t.work.pred) {
            val outRepl = coll.findOutReplica (edge.file.id)
            if (outRepl != null) {
                val id = IdGenerator.nextId ()
                this.coll.insertOrReplace (Replica (id, outRepl.f_id, t.loc.vm.node.name, t.id, ReplicaType.INPUT, FileState.SENDING, System.currentTimeMillis (), 0))
                ref ! MonitorProto.SendFile (outRepl.r_id, id, t.loc.vm.node.name, t.id)
            } else if (edge.task == null) { // Input file
                val id = IdGenerator.nextId ()
                this.coll.insertOrReplace (Replica (id, edge.file.id, t.loc.vm.node.name, t.id, ReplicaType.INPUT, FileState.SENDING, System.currentTimeMillis (), 0))
                ref ! MonitorProto.SendInput (edge.file.id, id, t.loc.vm.node.name, t.id)
            }
        }
    }

    def replicaTimeOut (repl : Replica, size : Int, anc : SchedTask, t : SchedTask) : Boolean = {
        if (repl.state == FileState.SENT) return false
        if (t.loc == null) return false;

        val bw = if (anc != null) {
            infra.bw (anc.loc.vm.node.cluster) (t.loc.vm.node.cluster)
        } else {
            infra.bw ("input") (t.loc.vm.node.cluster)
        }

        val time = (10 + (size / bw)) * 1000
        val taken = System.currentTimeMillis () - repl.creation

        if (taken > time) {
            println (s"Time out file : $taken , $time ! ${repl.f_id}, ${repl.r_id}")
            true
        } else false    
    }

    def minStartTime (task : SchedTask, node : SchedNode) : Long = {
        var time : Long = (System.currentTimeMillis () / 1000) - startSchedule
        for (edge <- task.work.pred) {
            if (edge.task != null) {
                val end = edge.task.loc.end
                val bw = infra.bw (edge.task.loc.vm.node.cluster) (node.cluster)
                val bw_time = ceil ((edge.file.size // * 10
                ) / bw.toDouble).toLong

                if (time < end + bw_time)
                    time = end + bw_time            
            } else {
                val bw = infra.bw ("input")(node.cluster)
                val bw_time = ceil ((edge.file.size // * 10
                ) / bw.toDouble).toLong

                if (time < bw_time)
                    time = bw_time
            }
        }
        time
    }

    def currentTime () : Long = {
        (System.currentTimeMillis () / 1000) - startSchedule
    }

    /**
      * Placement bcp plus facile, la VM a une taille fixe, on la fais juste glisser, comme on le ferais pour une tâche bare metal
      */

    def log (str : String) : Unit = {
        import java.util.Calendar
        import java.text.SimpleDateFormat

        val dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssX")
        val now = Calendar.getInstance().getTime ()
        val log = Log (dateFormat.format (now), str)
        //println (Formatter.createTable[Log] (Seq (log)))
    }


    def removeAllDirs (flow : Workflow) : Unit = {
    }

    def getFlowOfFile (id : Long) : Workflow = {
        val flows = this.load.filter (x => x.hasFile (id))
        if (flows.length != 0)
            flows (0)
        else null
    }

    def getFile (id : Long) : SchedFile = {
        val files = this.load.map (x => x.getFile (id)).reduce (_ ++ _)
        if (files.length != 0)
            files (0)
        else null
    }

    def printNodes () : Unit = {
        try {
            for (n <- infra.nodes)
                println (n._2)
            var time : Long = (System.currentTimeMillis () / 1000) - startSchedule
            println ("Time : " + time)
        } catch {
            case e : Exception =>
                e.printStackTrace ()
                System.exit (-1)
        }
    }

    def printNodeUsage () : Unit = {
        for (n <- infra.nodes) {
            print (n._1 + " -> " + n._2.getCurrentUsage ())
            println ( ": " + n._2.isValid ())
            if (!n._2.isValid ())
                System.exit (-1)
        }
    }

    def printReplicas () : Unit = {
    }



}


