package com.orch.scheduler

object SchedFileType {
    val NONE : Int = 0
    val INPUT : Int = 1
    val OUTPUT : Int = 2
}

case class SchedFile (id : Long, name : String, size : Int, isReady : Boolean, io : Int = 0)

