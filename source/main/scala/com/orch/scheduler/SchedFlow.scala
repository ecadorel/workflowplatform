package com.orch.scheduler
import com.github.mdr.ascii.layout._
import com.github.mdr.ascii.graph._
import com.orch.db._
import java.nio.file.{Files, Paths}
import com.orch.utils._


case class Workflow (id : Long, user : String, path : String, tasks : Map [Long, SchedTask], files : Map [Long, SchedFile], deadline : Long, var start : Long = 0) {

    def toGraph () : Graph[String] = {
        var vertices : Set [String] = Set ()
        var edges : List [(String, String)] = List ()

        for (t <- tasks) {
            vertices += ("" + t._2.id + ":" + t._2.work.app + ":" + t._2.meta)
        }

        vertices += ("input")
        vertices += ("output")

        for (t <- tasks) {
            for (out <- t._2.work.succ) {
                if (out.task != null && vertices.contains ("" + out.task.id + ":" + out.task.work.app + ":" + out.task.meta))
                    edges = edges :+ ("" + t._2.id + ":" + t._2.work.app + ":" + t._2.meta -> ("" + out.task.id + ":" + out.task.work.app + ":" + out.task.meta))
                else if (out.file.io == SchedFileType.OUTPUT)
                    edges = edges :+ ("" + t._2.id + ":" + t._2.work.app + ":" + t._2.meta -> "output")
            }

            for (in <- t._2.work.pred) {
                if (in.file.io == SchedFileType.INPUT)
                    edges = edges :+ ("input" -> ("" + t._2.id + ":" + t._2.work.app + ":" + t._2.meta))
            }
        }

        Graph (vertices, edges)
    }

    def getTask (id : Long) : Option [SchedTask] = {
        if (tasks.contains (id))
            Some (tasks (id))
        else None
    }

    def getAllSchedubale () : Seq [SchedTask] = {
        var list : List [SchedTask] = List ()
        for (t <- tasks) {
            if (t._2.isSchedulable () && t._2.loc == null)
                list = t._2 :: list
        }
        list
    }

    def getAllUnschedulable () : Seq [SchedTask] = {
        var list : List [SchedTask] = List ()
        for (t <- tasks) {
            if (!t._2.isSchedulable () && t._2.loc == null)
                list = t._2 :: list
        }
        list
    }

    def inputs () : Seq [SchedFile] = {
        files.values.filter (x => x.io == SchedFileType.INPUT).to[Seq]
    }

    def outputs () : Seq [SchedFile] = {
        files.values.filter (x => x.io == SchedFileType.OUTPUT).to[Seq]
    }

    def hasFile (id : Long) : Boolean = {
        files.values.filter (x => x.id == id).to [Seq].length != 0
    }

    def getFile (id : Long) : Seq [SchedFile] = {
        files.values.filter (x => x.id == id).to [Seq]
    }

    def isFinished () : Boolean = {
        var n_f = 0
        var f = 0
        for (t <- tasks) {
            if (t._2.loc == null || t._2.loc.state != TaskState.FINISHED) {
                n_f += 1
            } else {
                f += 1
            }
        }
        if (n_f != 0) {
            println (s"Flow ${id} is not finished $user ${(f * 1.0 / (n_f + f) * 1.0) * 100.0} at ${deadline}")
            false
        } else
              true
    }

    override def toString () : String = {
        val graph = this.toGraph ()
        "Flow : " + id + "\n" + GraphLayout.renderGraph(graph)
    }
    
}


object Workflow {

    import java.io._
    import com.orch.utils._
    import java.util.{Map => JMap, List => JArray}
    import org.yaml.snakeyaml.Yaml
    import scala.collection.JavaConverters._

    def loadFile (file : String) : (List [Workflow], Map [String, (String, String, Int, Int)]) = {
        val yaml : Yaml = new Yaml
        val root = yaml.load (new FileInputStream (new java.io.File (file)))
        val map = root.asInstanceOf [JMap[String, Object]].asScala
        val flows = map ("workload")
        var load : List [Workflow] = List ()
        var oss : Map [String, (String, String, Int, Int)] = Map ()

        for (obj <- flows.asInstanceOf [JArray [Object]].asScala) {
            val flow = obj.asInstanceOf [JMap[String, Object]].asScala
            val name = flow ("name").asInstanceOf [String]
            val user = flow ("user").asInstanceOf [String]

            val deadline = if (flow.contains ("deadline"))
                flow ("deadline").asInstanceOf [Int]
            else Int.MaxValue

            val start = if (flow.contains ("start"))
                flow ("start").asInstanceOf [Int]
            else 0

            val flow_ = loadFlow (name, user, deadline)
            if (start != 0) flow_.start = start

            load = flow_ :: load
            oss = oss ++ loadOss (name)            
        }

        (load, oss)
    }

    def findCreator (file : String, in_outs : Map [Long, (Seq [String], Seq [String])]) : Option [Long] = {
        for (z <- in_outs) {
            if (z._2._2.find (_ == file) != None) return Some (z._1)
        }
        None
    }

    def loadFiles (root : Object) : Map [String, SchedFile] = {
        var file_ids : Map [String, SchedFile] = Map ()
        for (obj <- root.asInstanceOf[JArray[Object]].asScala) {
            val file = obj.asInstanceOf [JMap[String, Object]].asScala
            val name = file ("id").asInstanceOf [String]
            val size = file ("size").asInstanceOf [Int]
            val path = file ("name").asInstanceOf [String]
            var io = SchedFileType.NONE
            if (file.contains ("type")) {
                var io_type = file ("type").asInstanceOf [String]
                io = if (io_type == "input") SchedFileType.INPUT
                else if (io_type == "output") SchedFileType.OUTPUT
                else SchedFileType.NONE
            }

            val id = IdGenerator.nextId ()
            file_ids += (name -> SchedFile (id, path, size, false, io))
        }
        file_ids
    }

    def loadTasks (user : String, root_name : String, root : Object) : (Map [Long, SchedTask], Map [Long, (Seq [String], Seq [String])]) = {
        var in_outs : Map [Long, (Seq [String], Seq [String])] = Map ()
        var tasks : Map [Long, SchedTask] = Map ()
        for (obj <- root.asInstanceOf [JArray [Object]].asScala) {
            val task = obj.asInstanceOf [JMap[String, Object]].asScala
            val name = Path.build (Path (root_name), task ("app").asInstanceOf [String]).file
            val len = task ("mean_len").asInstanceOf [Int]
            val dev = task ("dev_len").asInstanceOf [Int]
            var params = ""
            if (task.contains ("params"))
                params = task ("params").asInstanceOf [String]

            val os = task ("os").asInstanceOf [String]
            var needs : Map [String, Int] = Map ()
            for (need <- task ("needs").asInstanceOf [JMap[String, Object]].asScala)
                needs += (need._1 -> need._2.asInstanceOf [Int])

            var input : Array[String] = Array ()
            var output : Array [String] = Array ()
            for (in <- task ("input").asInstanceOf [JArray [String]].asScala) {
                input = input :+ in
            }

            for (out <- task ("output").asInstanceOf [JArray [String]].asScala) {
                output = output :+ out
            }

            val id = IdGenerator.nextId ()
            in_outs += (id -> (input, output))
            tasks += (id -> new SchedTask (id, new WorkTask (name, params, len, dev, needs, os, user, Seq (), Seq ()), null, null))
        }
        (tasks, in_outs)
    }

    def constructFlow (user : String, path_name : String, tasks : Map [Long, SchedTask], in_outs : Map [Long, (Seq[String], Seq [String])], files : Map[String, SchedFile], deadline : Int) : Workflow = {
        for (t <- tasks) {
            for (in <- in_outs (t._2.id)._1) {
                var name = findCreator (in, in_outs)
                name match {
                    case Some (id) =>
                        val pred = tasks (id)
                        t._2.work.addPred (Edge (files (in), pred))
                        pred.work.addSucc (Edge (files (in), t._2))
                    case None => {
                        val file = files (in)
                        t._2.work.addPred (Edge (file, null))
                    }
                }
            }

            for (out <- in_outs (t._2.id)._2) {
                if (files (out).io == SchedFileType.OUTPUT) {
                    val file = files (out)
                    val out_file = SchedFile (file.id, file.name, file.size, true, SchedFileType.OUTPUT)
                    t._2.work.addSucc (Edge (out_file, null))
                }
            }
        }

        var ret_files : Map [Long, SchedFile] = Map ()
        for (f <- files)
            ret_files += (f._2.id -> f._2)

        val id = IdGenerator.nextId ()
        for (x <- tasks)
            x._2.setFamily (Workflow (id, user, path_name, tasks, ret_files, deadline))

        Workflow (id, user, path_name, tasks, ret_files, deadline)
    }

    def loadFlow (name : String, user : String, deadline : Int) : Workflow = {
        val yaml = new Yaml
        val root = yaml.load (new FileInputStream (new java.io.File (Path.build (name, "flow.yaml").file)))
        val map = root.asInstanceOf [JMap[String, Object]].asScala


        var file_ids = loadFiles (map ("files"))
        var (tasks, in_outs) = loadTasks (user, name, map ("tasks"))

        constructFlow (user, name, tasks, in_outs, file_ids, deadline)
    }

    def loadOss (name : String) : Map [String, (String, String, Int, Int)] = {
        val path = Path.build (name, "custom_img.yaml").file
        var oss : Map [String, (String, String, Int, Int)] = Map ()
        if (Files.exists (Paths.get (path))) {
            val yaml = new Yaml
            val root = yaml.load (new FileInputStream (new java.io.File (path)))
            val images = root.asInstanceOf [JMap[String, Object]].asScala ("images")
            for (obj <- images.asInstanceOf [JArray[Object]].asScala) {
                val image = obj.asInstanceOf [JMap [String, String]].asScala
                val name = image ("name")
                val os = image ("os")
                val script = image ("script")
                val mean = image ("mean_len").asInstanceOf [Int]
                val dev = image ("dev_len").asInstanceOf [Int]
                oss += (name -> (os, if (script == null) "" else script, mean, dev))
            }            
        }
        return oss
    }

}
