package com.orch.scheduler

import com.orch.db._
import com.orch.scheduler._
import scala.math.{max, min, ceil}
import com.orch.utils._

class SchedVM (var id : Long, val node : SchedNode, val user : String, val os : String, val boot : Long) {

    var state : Int = VMState.DOWN // Etat de la VM

    var start : Long = Long.MaxValue // Start pour l'ordonnancement 
    var end   : Long = 0 // End pour l'ordonnancement
    var begin : Long = 0

    var usage : Map [String, Seq [Interval]] = Map () // Usage
    var capas : Map [String, Int] = Map () // Capas, figé quand on passe à l'état boot ou running
    var tasks : Map [Long, SchedTask] = Map () // les tâches présentes sur la vm
    var ords  : Seq [SchedTask] = Seq ()

    var _isBestEffort : Boolean = false

    def changeId () {
        this.id = IdGenerator.nextId ()
    }

    def setBestEffort () : Unit = {
        this._isBestEffort = true
    }

    def isBestEffort () : Boolean = {
        this._isBestEffort 
    }

    /**
      *  Partie ordonnancement
      */

    def getBootTime () : Long = {
        if (state == VMState.DOWN || state == VMState.BOOTING)
            boot
        else 0
    }

    def getUsage (caps : String) : Seq [Interval] = {
        if (this.usage.contains (caps))
            return this.usage (caps)
        else return Seq ()
    }

    def canRun (task : SchedTask) : Boolean = {
        if (state == VMState.KILLING || state == VMState.KILLED) return false
        if (user != task.work.user || os != task.work.os) return false
        if (state == VMState.DOWN) return true

        for (n <- task.work.needs) {
            if (!capas.contains (n._1)) return false
        }

        return true
    }

    /**
      *  Verifie que la VM n'est pas full
      */
    def isFull (nodeUsage : Seq [Interval], nodeCapas : Int, taskLen : Long, taskNeed : Int) : Boolean = {
        val af = Interval.heightAt (this.end, nodeUsage)
        val bef = Interval.heightAt (this.start, nodeUsage)

        if (af == nodeCapas && bef == nodeCapas && this.usage.contains ("cpus")) {
            val usage = this.usage ("cpus")
            val max = if (this.state == VMState.DOWN) {
                val set = Interval.subset (nodeUsage, this.start, this.end)
                val max = Interval.maxIntervalOnIntersect (set)
                nodeCapas - max
            } else this.capas ("cpus")

            val inverse = Interval.maxLen (Interval.inverse (usage, max))
            if (inverse >= taskLen) return false            

            true
        } else false // peut être pas, mais compliqué à calculer
    }


    /**
      *  Verifie que les capacité ne font pas changer la taille de la VM si elle est running ou boot
      */
    def isValid (caps : Map [String, Int]) : Boolean = {
        if (state == VMState.DOWN) return true
        for (x <- caps) {
            if (!capas.contains (x._1)) return false
            if (capas (x._1) < x._2) return false
        }

        return true
    }

    def isValid () : Boolean = {
        if (state == VMState.DOWN) return true
        for (x <- usage) {
            val max = Interval.maxIntervalOnIntersect (x._2)
            if (capas (x._1) < max) return false
        }
        return true
    }

    def getPlace (zero : Long, startTime : Long, task : SchedTask) : LocTask = {
        if (!canRun (task)) return null
        if (this.tasks.size == 0 && this.state == VMState.DOWN) return getPlaceOnNewVM (zero, startTime, task)

        val boot = getBootTime ()
        var position = max (startTime, boot + zero)
        var lastPosition : Long = position
        if (this.state != VMState.DOWN)
            position = max (start + boot, startTime)

        val nodeUsage = node.getUsageWithout (this)

        val nodeCapas = node.getCapas ()
        val len = ceil (task.work.computeLen (Utils.PROBA) / (node.getSpeed * Utils.SPEED_DEGRAD)).toLong + Utils.TIME_ORDER
        var i = 0
        while (true) {
            i += 1
            val loc = new LocTask (position, position + len, TaskState.NONE, task, this)
            val (start, end, newUsage, newCapas) = fakeValidate (loc)

            // Si la VM est on, il faut que ses capas soit les bonne
            val vmCapasIfAdd = if (state == VMState.DOWN) newCapas else if (isValid (newCapas)) capas else newCapas

            var over = lastOver (start - boot, end, vmCapasIfAdd, nodeUsage, nodeCapas)            
            if (over.begin == -1 && isValid (newCapas)) {
                return loc
            } else {                
                if (diffCapas (this.capas, newCapas)) {
                    val max_capas = if (state == VMState.DOWN) {
                        computeMaxCapas (start, end, this.capas, nodeUsage, nodeCapas)
                    } else this.capas

                    if (max_capas == null) // On est obliger de diminuer la taille de la VM, donc la tache ne tiendra jamais
                        return null

                    val inner_over = lastOver (position, position + len, task.work.needs, this.usage, max_capas)

                    if (inner_over.begin != -1)
                        over = inner_over
                }
                

                if (over.end <= this.start - boot || over.begin < this.start)
                    position = over.end + boot
                else position = over.end


                if (i > 100 || lastPosition >= position) {
                    return null
                }

                lastPosition = position
            }
        }

        null
    }

    def diffCapas (left : Map [String, Int], right : Map [String, Int]) : Boolean = {
        for (x <- left) {
            if (!right.contains (x._1) || right (x._1) != x._2) return true
        }
        return false
    }


    def getPlaceOnNewVM (zero : Long, startTime : Long, task : SchedTask) : LocTask = {
        val len = ceil (task.work.computeLen (Utils.PROBA) / (node.getSpeed * Utils.SPEED_DEGRAD)).toLong + Utils.TIME_ORDER
        val boot = getBootTime ()
        

        val nodeCapas = this.node.getCapas ()
        val nodeUsage = this.node.getCurrentUsage()
        var position = max (zero, startTime - boot)

        if (nodeUsage.contains ("cpus"))
            position = Interval.firstUnder (position, nodeUsage ("cpus"), nodeCapas ("cpus") - task.work.needs ("cpus"))

        var lastPosition = position

        var i = 0
        while (true) {
            i += 1;
            val loc = new LocTask (position + boot, position + len + boot, TaskState.NONE, task, this)
            val (start, end, newUsage, newCapas) = fakeValidate (loc)

            // Pas besoin de verifier, la VM est forcement DOWN
            var over = lastOver (position, position + len + boot, newCapas, nodeUsage, nodeCapas)
           
            if (over.begin == -1) {
                return loc
            } else {
                position = over.end
            }

            if (i > 100 || lastPosition >= position) {
                position = nodeUsage ("cpus").sortWith (_.end > _.end) (0).end
                return new LocTask (position, position + len, TaskState.NONE, task, this)
            }

            lastPosition = position
        }

        null 
    }


    def computeMaxCapas (start : Long, end : Long, oldCapas : Map [String, Int], usage : Map [String, Seq [Interval]], K : Map [String, Int]) : Map [String, Int] = {
        var res : Map [String, Int] = Map ()
        for (k <- K) {
            if (usage.contains (k._1)) {
                val set = Interval.subset (usage (k._1), start, end)
                val max = Interval.maxIntervalOnIntersect (set)
                if (k._2 - max < oldCapas (k._1)) return null                
                res += (k._1 -> (k._2 - max))
            } else {
                if (k._2 < oldCapas (k._1)) return null
                res += (k._1 -> k._2)
            }
        }
        res
    }

    def firstOver (start : Long, end : Long, add : Map [String, Int], usage : Map [String, Seq [Interval]], K : Map [String, Int]) : Interval = {
        var endOver = Interval (-1, -1, 0)
        for (u <- add) {
            if (usage.contains (u._1)) {
                var local_usage : Seq [Interval] = usage (u._1)
                val local_end = Interval.firstOver (start, end, local_usage, u._2, K (u._1))
                if (local_end.end > endOver.end)
                    endOver = local_end
            }
        }

        endOver
    }

    def lastOver (start : Long, end : Long, add : Map [String, Int], usage : Map [String, Seq [Interval]], K : Map [String, Int]) : Interval = {
        var endOver = Interval (-1, -1, 0)
        for (u <- add) {
            if (usage.contains (u._1)) {
                for (j <- usage (u._1))
                    if (j.end >= start) {
                        val x = max (j.begin, start)
                        val y = min (j.end, end);
                        if (x < y) {
                            if (endOver.end < j.end && (j.height + u._2) > K (u._1)) {
                                endOver = new Interval (x, j.end, j.height + u._2)
                        }
                    }
                    }
            }            
        }

        endOver
    }

    /**
      * Partie ajout suppression de tâche
     */

    def addTask (loc : LocTask) : Unit = {
        this.tasks += (loc.task.id -> loc.task)
        this.ords = this.tasks.values.to [Seq].sortWith (_.loc.start < _.loc.start)
        val (start, end, current, capas) = this.fakeValidate (loc)
        this.start = start
        this.end = end
        this.usage = current

        if (this.state == VMState.DOWN)
            this.capas = capas
        
        this.node.addVM (this);
    }

    def getSortedTasks () : Seq [SchedTask] = {
        return this.ords
    }

    def addTaskFast (loc : LocTask) : Unit = {
        this.tasks += (loc.task.id -> loc.task)
    }

    def removeTask (loc : LocTask) : Unit = {
        this.tasks = this.tasks.filterKeys (_ != loc.task.id)
        validate ()
        this.node.addVM (this)
        if (this.tasks.size == 0 && this.state == VMState.DOWN) {
            this.node.removeVM (this)
        }
    }

    def validatePostRemoveFast () : Unit = {
        validate ()
        this.node.addVM (this)
        if (this.tasks.size == 0 && this.state == VMState.DOWN) {
            this.node.removeVM (this)
        }
    }

    def removeTaskFast (loc : LocTask) : Unit = {
        this.tasks = this.tasks.filterKeys (_ != loc.task.id)
    }

    def validate () : Unit = {
        val (start, end, usage, capas) = computeUsageAndCapas ()

        this.usage = usage

        if (this.state == VMState.DOWN) 
            this.capas = capas
        else if (!this.isValid (capas)) {
            println (capas);
            println (this.capas)
            println (this.usage)
            println (this)
            println ("NOOOOONN")
            throw new Exception ("")
        }

        this.start = start
        this.end   = end        
    }

    def fakeValidate (loc : LocTask) : (Long, Long, Map [String, Seq [Interval]], Map [String, Int]) = {
        val fakeTask = new SchedTask (loc.task.id, loc.task.work, loc, loc.task.meta);
        fakeTask.loc = loc

        var currentLoad = this.usage
        for (n <- fakeTask.work.needs) {
            val array = (if (this.usage.contains (n._1)) this.usage (n._1) else Seq ()) :+ new Interval (loc.start, loc.end, n._2)
            currentLoad = currentLoad + (n._1 -> Interval.allIntersect (array))
        }

        var capas : Map[String, Int] = Map ()
        for (n <- currentLoad) {
            capas = capas + (n._1 -> Interval.maxIntervalOnIntersect (n._2))
        }

        var end = if (loc.end > this.end)
            loc.end
        else this.end        

        var start = if (loc.start < this.start)
            loc.start
        else this.start

        if (start == Long.MaxValue)
            start = end

        if (this.state == VMState.RUNNING || this.state == VMState.BOOTING)
            start = 0 + getBootTime ()

        if (this.start == VMState.BOOTING && end == 0)
            end = getBootTime ()

        (start, end, currentLoad, capas)
    }

    def fullValidate () : Unit = {
        val (start, end, usage, capas) = computeUsageAndCapas ()

        this.usage = usage

        if (this.state == VMState.DOWN) 
            this.capas = capas

        this.start = start   
        this.end   = end

        this.node.addVM (this)
        if (this.tasks.size == 0 && (this.state == VMState.DOWN || this.state == VMState.KILLED)) {
            this.node.removeVM (this)
        }
    }


    def computeUsageAndCapas () : (Long, Long, Map [String, Seq [Interval]], Map [String, Int]) = {
        var raw : Map [String, Seq[Interval]] = Map ()
        var start : Long = Long.MaxValue
        var end   : Long = 0

        for (t <- this.tasks) {
            for (n <- t._2.work.needs) {
                if (raw.contains (n._1)) {
                    raw += (n._1 -> (raw (n._1) :+ Interval (t._2.loc.start, t._2.loc.end, n._2)))
                } else {
                    raw += (n._1 -> Seq (Interval (t._2.loc.start, t._2.loc.end, n._2)))
                }
            }

            if (start > t._2.loc.start) start = t._2.loc.start
            if (end < t._2.loc.end)     end = t._2.loc.end
        }

        var usage : Map [String, Seq [Interval]] = Map ()
        var capas : Map [String, Int] = Map ()
        for (r <- raw) {
            val use = Interval.allIntersect (r._2)
            usage += (r._1 -> use)
            capas += (r._1 -> Interval.maxIntervalOnIntersect (use))
        }        

        if (start == Long.MaxValue) {
            start = end 
        }

        if (this.state == VMState.RUNNING || this.state == VMState.BOOTING) {
            start = 0 + getBootTime ()
            capas = this.capas
        }
        
        if (this.state == VMState.BOOTING && end == 0)
            end = getBootTime ()

        if (this.tasks.size == 0 && start == end) {
            start = 0
            end = 0
        }

        if (start == -1) {
            println (this + " " + this.tasks.map (x => (x._2.loc.start, x._2.loc.end)))
            System.exit (-1)
        }

        (start, end, usage, capas)
    }

    /**
      *  Partie pour le calcul exterieur (node, sched...)
      */

    def toInterval (capa : String) : Interval = {
        if (this.capas.contains (capa)) {
            val boot = getBootTime ()
            if (end == start)
                return Interval (start, end, this.capas (capa))
            Interval (start - boot, end, this.capas (capa))
        } else Interval (0, 0, 0)
    }

    def getStart () : Long = {
        start
    }

    def getEnd () : Long = {
        end
    }

    def getLength () : Long = {
        return end - start
    }

    def getState () : Int = {
        state
    }

    def getCapas () : Map [String, Int] = {
        return capas
    }

    def getCapa (cp : String) : Int = {
        return capas (cp)
    }

    def getUsage () : Map [String, Seq [Interval]] = {
        return this.usage
    }

    def setLaunch (current : Long) : Unit = {
        this.state = VMState.BOOTING
        this.begin = current
        this.fullValidate ()
    }

    def getBegin () : Long = {
        begin
    }

    def setRunning () : Unit = {
        this.state = VMState.RUNNING
        this.fullValidate ()
    }

    def setOff () : Unit = {
        this.state = VMState.DOWN
        this.fullValidate ()
    }

    def setKill () : Unit = {
        this.state = VMState.KILLED
        this.fullValidate ()
    }

    def setKilling () : Unit = {
        this.state = VMState.KILLING
    }

    def setPause () : Unit = {
        this.state = VMState.PAUSE
    }

    def setResume () : Unit = {
        this.state = VMState.RESUMING
    }

    def getTasks () : Map [Long, SchedTask] = {
        tasks
    }

    def purge () : Unit = {
        tasks = Map ()
        //fullValidate ()
    }

    override def toString () : String = {        
        return "[" + this.boot + ":" + this.toInterval ("cpus").toString () + ":" + this.user + ':' + this.state + ':' + this.tasks.size + " is " + this._isBestEffort + ']'
    }
    
}
