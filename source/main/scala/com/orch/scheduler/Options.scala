package com.orch.scheduler
import scopt.OParser

object Options {

    case class Config(
        addr : String = "127.0.0.1",
        lport : Int = 5500,
        rport : Int = 5100,
        file : String = "apps/workload.yaml",
        schType : Int = 0,
        alpha : Double = 1.0
    )


    val builder = OParser.builder[Config]

    val parser1 = {
        import builder._
        OParser.sequence(
            programName("shell"),
            head("scopt", "4.x"),
            // option -f, --foo
            opt[String]('a', "addr")
                .action ((x, c) => {
                    c.copy (addr=x)
                })
                .text (""),
            
            opt[Int]('p', "local-port")
                .action ((x, c) => c.copy (lport=x))
                .text ("local port, used to remote access to this local node"),

            opt[Int]("remote-port")
                .action ((x, c) => c.copy (rport=x))
                .text ("remote port, of the master node"),

            opt [String]("load")
                .action ((x, c) => c.copy (file=x))
                .text ("config file of the load"),

            opt [Int]("type")
                .action ((x, c) => c.copy (schType=x))
                .text ("type of scheduler to use : \n\t 0 : Remove\n\t 1 : HEFT"),

            opt [Double]("alpha")
                .action ((x, c) => c.copy (alpha=x))
                .text ("alpha")
        )
    }

}
