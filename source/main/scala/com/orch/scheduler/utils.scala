package com.orch.scheduler

object Utils {
    val PROBA        : Double = 0.5
    val PROBA_VM     : Double = 0.5
    val SPEED_DEGRAD : Double = 1
    val TIME_ORDER   : Long   = 0
    var ALPHA : Double = 20.0
}
