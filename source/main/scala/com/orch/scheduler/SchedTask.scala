package com.orch.scheduler

import com.orch.db._


case class Edge (file : SchedFile, task : SchedTask)

class LocTask (var start : Long, var end : Long, var state : Int, val task : SchedTask, val vm : SchedVM) {
    val creation = System.currentTimeMillis () / 1000

    def apply () : Unit = {
        this.task.loc = this
        this.vm.addTask (this)
        // if (!this.vm.node.isValid ()) {
        //     println (this.vm)
        //     println (this.vm.node)
        //     println (this.vm.node.getCurrentUsage ())
        //     println ("Not valid")
        //     this.vm.removeTask (this)
        //     println (this.vm)
        //     println (this.vm.node)
        //     println (this.vm.node.getCurrentUsage ())

        //     val nodeUsage = vm.node.getUsageWithout (this.vm)
        //     val nodeCapas = vm.node.getCapas ()
        //     val (start, end, newUsage, newCapas) = this.vm.fakeValidate (this)
        //     var over = vm.lastOver (start - this.vm.getBootTime (), end, newCapas, nodeUsage, nodeCapas)
        //     println (over)
        //     println (newCapas)
        //     println (start+ " " + end)

        //     System.exit (-1)
        // }
    }

    def applyFast () : Unit = {
        this.task.loc = this
        this.vm.addTaskFast (this)
    }

    def releaseFast () : Unit = {
        this.vm.removeTaskFast (this)
    }

    def totalReleaseFast () : (Seq [SchedTask], Seq [SchedVM]) = {
        var vms : Seq [SchedVM] = Seq ()
        var tasks : Seq [SchedTask] = Seq ()

        this.vm.removeTaskFast (this)
        this.task.loc = null
        vms = vms :+ this.vm

        for (s <- this.task.work.succ) {
            if (s.task != null && s.task.loc != null && s.task.loc.state == TaskState.NONE) {
                val (_tasks, _vms)  = s.task.loc.totalReleaseFast ()
                tasks = tasks ++ _tasks
                vms = vms ++ _vms
            }
        }
        
        (tasks :+ this.task, vms)
    }

    def release () : Unit = {
        this.vm.removeTask (this)
    }

    def totalRelease () : Seq [SchedTask] = {
        var list : Seq [SchedTask] = Seq ()
        this.vm.removeTask (this)
        this.task.loc = null
        for (s <- this.task.work.succ) {
            if (s.task != null && s.task.loc != null && s.task.loc.state == TaskState.NONE)
                list = list ++ s.task.loc.totalRelease ()
        }
        list :+ this.task
    }
}

class WorkTask (val app : String, val params : String, val len : Int, val dev : Int, val needs : Map [String, Int], val os : String, val user : String, var succ : Seq [Edge], var pred : Seq [Edge]) {

    import com.orch.utils._

    def computeLen (proba : Double = Utils.PROBA) : Long = {
        NormalDistribution (len, dev).inverseCDF (proba).toLong
    }

    def addSucc (edge : Edge) : Unit = {
        this.succ = this.succ :+ edge
    }

    def addPred (edge : Edge) : Unit = {
        this.pred = this.pred :+ edge
    }

}

trait MetaData {
    def isSchedulable (task : SchedTask) : Boolean = false
}

class SchedTask (val id : Long, var work : WorkTask, var loc : LocTask, var meta : MetaData) {

    var family : Workflow = Workflow (0, "", "", Map (), Map (), 0)

    def setFamily (fam : Workflow) : Unit = {
        this.family = fam
    }

    def getFamily () : Workflow = {
        return this.family
    }

    def isEntry () : Boolean = {
        for (x <- work.pred) {
            if (x.task != null) return false
        }

        return true
    }

    def isOut () : Boolean = {
        for (x <- work.succ) {
            if (x.task != null) return false
        }
        
        return true
    }

    /**
      *  Pour qu'on autorise une tâche a être ordonnancer il faut que tout les parents soient au minimum running
      */
    def isSchedulable () : Boolean = {
        if (meta.isSchedulable (this)) return true

        for (x <- work.pred) {
            if (x.task != null && (x.task.loc == null || x.task.loc.state == TaskState.NONE))
                return false
        }

        return true
    }


    def setNone () : Unit = {
        if (this.loc != null) {
            this.loc.state = TaskState.NONE
        } 
    }

    def setRunning () : Unit = {
        if (this.loc == null) {
            println ("[1;31m Launch not scheduled task ... [0m")
            System.exit (-1)
        } else this.loc.state = TaskState.RUNNING
    }

    def setFinished () : Unit = {
        if (this.loc == null) {
            println ("[1;31m Finish not scheduled task ... [0m")
            System.exit (-1)
        } else this.loc.state = TaskState.FINISHED
    }

}
