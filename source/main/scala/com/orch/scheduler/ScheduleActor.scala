package com.orch.scheduler

import java.io._
import java.util.{Map => JMap, List => JArray}
import org.yaml.snakeyaml.Yaml
import scala.collection.JavaConverters._
import akka.actor.LocalActorRef
import akka.actor.{Props, Actor, ActorSystem, ActorRef, ActorLogging, ActorSelection, PoisonPill }
import com.typesafe.config.ConfigFactory
import scala.math.{max}
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * On instancie un scheduler pour gérer un load
  * Un load est un fichier, qui regroupe un ensemble de workflow a executer  
  */
class ScheduleActor (LAddr : String, LPort : Int, RAddr : String, RPort : Int, schType : Int) extends Actor with ActorLogging {
    import com.orch.db._
    import com.orch.utils._
    import com.orch.master.MonitorProto._

    var master: ActorSelection = null
    var coll = new Collection ("orch")
    var scheduler : Scheduler = null
    var nbTreated : Int = 0
    var last : Long = System.currentTimeMillis ()
    val WINDOW : Int = 100
    var notDone = 0
    var fullNbTreated : Int = 0
    val mutex : Integer = new Integer (0)
    var futureLoad : List [Workflow] = List ()

    override def preStart () : Unit = {
        println ("Starting scheduler")
        println ("Register to master node")
        master = context.actorSelection (s"akka.tcp://RemoteSystem@$RAddr:$RPort/user/master")
        master ! MonitorProto.AddMonitor (LAddr, LPort)
        // schType match {
        //     case 1 =>
        scheduler = new HEFTScheduler (this, master, RAddr, RPort + 1)
        // case 2 =>
        //scheduler = new HEFTDeadScheduler (this, master, RAddr, RPort + 1)
            // case _ =>

        // V3Speed.ALGO_TYPE = REMOVE_ALGO_TYPES.ALL
        // V3Speed.KILLING   = 1
        // V3Speed.OBJ_TYPE  = REMOVE_ALGO_TYPES.SPEED
        // scheduler = new RemoveV3Scheduler (this, master, RAddr, RPort + 1)

        // V2Speed.ALGO_TYPE = REMOVE_ALGO_TYPES.ALL
        // V2Speed.KILLING   = 1
        // V2Speed.OBJ_TYPE  = REMOVE_ALGO_TYPES.ENERGY
        // scheduler = new RemoveV2Scheduler (this, master, RAddr, RPort + 1)

        // ND_ALGO_TYPES.ALGO_TYPE = ND_ALGO_TYPES.RATIO;
        // scheduler = new NearDeadlineScheduler (this, master, RAddr, RPort + 1)

        //}

    }

    def receive : Receive = {
        case MonitorProto.VmReady (id, _) =>
            this.mutex.synchronized {
                scheduler.onVmReady (id)
            }
            nbTreated += 1

        case MonitorProto.VmOff   (id, _) =>
            this.mutex.synchronized {
                scheduler.onVmOff (id)
            }
            nbTreated += 1

        case MonitorProto.TaskEnd     (id) =>
            this.mutex.synchronized {
                scheduler.onTaskFinished (id, mutex)
            }
            nbTreated += 1

        case MonitorProto.FileSent    (id) =>
            scheduler.onFileSent (id)            
            nbTreated += 1

        case SchedulerProto.AddLoad   (file) =>
	    println ("Add load")
            val (load, oss) = Workflow.loadFile (file)
            scheduler.addCustomOs (oss)
            for (f <- load) {
                if (f.start == 0)
                    scheduler.addLoad (List (f))
                else {
                    this.futureLoad = this.futureLoad :+ f
                }
            }
            
        case SchedulerProto.Start () =>
	    println ("Start")
            val future = new Thread () {
                override def run () : Unit = {
                    periodicSchedule ()
                }
            }

            future.start ()

        case MonitorProto.VmError (id, _) =>
            this.mutex.synchronized {
                scheduler.onVmError (id)
            }
            nbTreated += 1

        case MonitorProto.TaskFailure (id, _) =>
            this.mutex.synchronized {
                scheduler.onTaskError (id)
            }
            nbTreated += 1

        case MonitorProto.FileFailure (id) =>
            scheduler.onFileFailure (id)    
            nbTreated += 1

        case e =>
            println (s"[1;31m Unhandled $e [0m")

    }

    def periodicSchedule () : Unit = {
        var toWait : Long = 1000
        while (true) {
            println (s"Wait : $toWait")
            Thread.sleep (toWait)
            println ("[1;33mPeriodic schedule[0m")
            // On ne fait un schedule que si il n'y a plus de message dans la liste
            if (this.futureLoad.length != 0) {
                val time = this.scheduler.currentTime ()
                var aux : List [Workflow] = List ()
                var toAdd : List [Workflow] = List ()
                for (f <- this.futureLoad) {
                    if (f.start <= time)
                        toAdd = toAdd :+ f
                    else aux = aux :+ f
                }
                if (toAdd.length != 0) this.scheduler.addLoad (toAdd)
                this.futureLoad = aux
            }

            if (doSchedule () && this.futureLoad.length == 0) {
                stop ()
                return
            }            
        }
    }

    def doSchedule () : Boolean = {
        scheduler.scheduleAndRun (this.mutex)
        scheduler.isFinished ()        
    }

    def stop () : Unit = {
        master ! MonitorProto.RmMonitor (LAddr, LPort)
        self ! PoisonPill
        context.system.terminate ()
        println ("[1;35mEnd - Bye[0m")
    }

}

object ScheduleMain {
    import scopt.OParser

    def props (local_addr : String, local_port : Int, remote_addr : String, remote_port : Int, schType : Int) : Props =
        Props (new ScheduleActor (local_addr, local_port, remote_addr, remote_port, schType))

    def configFile (addr: String, port: Int):String = {
        s"""akka {
      log-dead-letters-during-shutdown=off
      log-dead-letters=off
      loglevel = "INFO"
      actor {
        warn-about-java-serializer-usage=off
        provider = "akka.remote.RemoteActorRefProvider"
      }
      remote {
        log-remote-lifecycle-events=off
        enabled-transports = ["akka.remote.netty.tcp"]
        netty.tcp {
          hostname = $addr
          port = $port
        }
        log-sent-messages = on
        log-received-messages = on
      }
      bounded-mailbox {
        mailbox-type = "akka.dispatch.BoundedMailbox"
        mailbox-capacity = 1000
        mailbox-push-timeout-time = 10s
      }
    }"""
    }

    def parseOptions (args : Array[String]) : Options.Config = {
        OParser.parse(Options.parser1, args, Options.Config()).getOrElse (Options.Config ())
    }

    def createSystem (local_addr : String, local_port : Int) : ActorSystem = {
        val config = ConfigFactory.parseString (configFile (local_addr, local_port))
        ActorSystem ("RemoteSystem", config)
    }

    def main (args : Array[String]) {
        val config = parseOptions (args)
        val system = createSystem (config.addr, config.lport)
        val sched = system.actorOf (
            ScheduleMain.props (config.addr, config.lport, config.addr, config.rport, config.schType),
            name="monitor"
        )
        Utils.ALPHA = config.alpha
        println (Utils.ALPHA)
        
        sched ! SchedulerProto.AddLoad (config.file)
        sched ! SchedulerProto.Start ()
    }

}
