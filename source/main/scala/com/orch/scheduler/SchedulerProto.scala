package com.orch.scheduler

object SchedulerProto {

    case class AddLoad (file : String)
    case class Start ()
}
