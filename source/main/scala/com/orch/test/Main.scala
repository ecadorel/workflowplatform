package com.orch.test
import com.orch.scheduler._
import akka.actor.{ Props, Actor, ActorSystem, ActorRef, ActorLogging, ActorSelection, PoisonPill }
import com.typesafe.config.ConfigFactory
import scala.math.{max}
import akka.stream.{ActorMaterializer, IOResult}
import java.nio.file.{ Path, Paths }
import akka.stream.scaladsl.{FileIO, Source}
import scala.concurrent.Future
import akka.util.ByteString
import java.nio.file.StandardOpenOption

import com.orch.scheduler._

object Test {

    def uniform (low : Int, high : Int) : Int = {
        val x = scala.util.Random.nextInt % (high - low)
        if (x < 0) -x + low
        else x + low
    }

    def main (args : Array[String]) {
        implicit val system = ActorSystem("QuickStart")
        implicit val materializer = ActorMaterializer()

        val file = Paths.get("greeting.txt")
        val text = Source.single("Hello Akka Stream!")
        val result: Future[IOResult] = text.map(t => ByteString(t)).runWith(FileIO.toPath(file, Set(StandardOpenOption.APPEND, StandardOpenOption.CREATE)))
        val result2: Future[IOResult] = text.map(t => ByteString(t)).runWith(FileIO.toPath(file, Set(StandardOpenOption.APPEND, StandardOpenOption.CREATE)))
    }


}
