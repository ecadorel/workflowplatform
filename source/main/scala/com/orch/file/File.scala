package com.orch.file
import java.net.ServerSocket
import java.io.IOException
import akka.actor.{ Actor, ActorRef, Props , ActorContext, ActorSelection }
import scala.collection.mutable.Queue

/**
  * Alors c'est un peu compliqué
  * On peut pas lancer trop de future en même temps
  * Et on peut pas non plus attendre à chaque fois, ça créer des deadlock quand on envoi un fichier à soi-même
  * Donc on a un queue de taille < nb_slot, si on doit rajouter un slot -> taille >= nb_slot, on await le premier, et on le pop
  * Il faut 2 queue, pour éviter que l'envoi bloque la réception, par exemple 4 envoi, en local = deadlock
  */
object FileIO {
    import com.orch.utils.{Path, Exe}
    val nb_slot = 10
    var nb_sending = 0
    var nb_recv = 0
    var current_recv : Map [Int, (Exe, Long)] = Map ()
    var current_send : Map [Int, (Exe, Long)] = Map ()

    var send_list : Queue [Exe] = Queue ()
    var used_port : List [Int] = List ()


    def uniform (low : Int, high : Int) : Int = {
        val x = scala.util.Random.nextInt % (high - low)
        if (x < 0) -x + low
        else x + low
    }

    def launchSend () : Unit = {
        this.send_list.synchronized {
            if (nb_sending < nb_slot && send_list.length != 0) {
                val fst = send_list.dequeue
                //println (s"[1;32mSending Place free ${fst._4} , $nb_sending [0m")
                nb_sending += 1
                fst.start ()
            } else {
                println (s"[1;33mMust wait Place free $nb_sending, $nb_slot, ${send_list.length} [0m")
            }
        }
    }

    def startServer () {
        new Exe (Seq ("ruby", "-run", "-ehttpd", "/", "-p8000"), Path ("."),
            (_,_,_) => {},
            (msg,_) => {
                println (msg)
                false
            }
        ).start ()
    }

    def onSendEnd () : Unit = {
        this.send_list.synchronized {            
            nb_sending -= 1
            launchSend ()
        }

        // this.used_port.synchronized {
        //     this.used_port = this.used_port.filter (_ != port)
        // }
    }

    def recvFile (ref : ActorRef, id : Long, srcPath : String, dstPath : String, DAddr : String) : Unit = {
        val path = parsePath (dstPath)
        val file = new java.io.File (path);
        val dir = file.getParent ()
        val exe = new Exe (Seq ("mkdir", "-p", dir), Path ("."), 
            (code, out, err) => {
                new Exe (Seq ("curl", DAddr + ":8000" + srcPath, "-o", path), Path("."),
                    (code, out, err) => {
                        println (s"[1;32mFileSuccess ${id}[0m")
                        ref ! FileProto.FileSent (id)
                        onSendEnd ()
                    },
                    (msg,_) => {
                        println (s"[1;31mFileError ${id}[0m")
                        println (msg)
                        false
                    }
                ).start ()
            },
            (msg, _) => {                
                false
            }
        )

        this.send_list.synchronized {
            send_list.enqueue (exe)
        }
        launchSend ()
    }

    def recvExe (remote : ActorSelection, id : Long, e_id : String, args : String, srcPath : String, dstPath : String, DAddr : String) : Unit = {
        // On lance un serveur sur un port aléatoire non utilisé
        val path = parsePath (dstPath)
        val file = new java.io.File (path);
        val dir = file.getParent ()
        val exe = new Exe (Seq ("mkdir", "-p", dir), Path ("."),
            (code, out, err) => {
                val exe = new Exe (Seq ("curl", DAddr + ":8000" + srcPath, "-o", path), Path("."),
                    (code, out, err) => {
                        println (s"[1;32mExeSuccess ${id}${e_id}[0m")
                        remote ! FileProto.ExeSent (id, args)
                    },
                    (msg,_) => {
                        println (msg)
                        false
                    }
                ).start ()
            },
            (msg, _) => {
                false
            }
        )

        // L'envoi d'exec est prioritaire, pour éviter un engorgement réseau qui bloquerait l'exécution des apps
        // this.send_list.synchronized {
        //     send_list = Queue (exe) ++ send_list
        // }
        //launchSend ()

        exe.start ()
    }

    def parsePath (name : String) : String = {
        val parts = name.split (":")
        if (parts (0) == "exec") {
            val begin = "/tmp/execs"
            val file = parts (1)
            Path.build (Path (begin), file).file
        } else if (parts (0) == "output") {
            parts (1)
        } else {
            val begin = "/tmp/"
            val user = parts (1)
            val task = parts (2)
            val file = parts (3)
            Path.build (Path.build (Path.build (Path (begin), user), task), file).file
        }
    }


}
