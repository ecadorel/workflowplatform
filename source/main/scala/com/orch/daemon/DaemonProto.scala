package com.orch.daemon.DaemonProto

object DaemonProto {

    case class RegisterMaster (addr: String, port : Int)

    /**
      * Message pour les VMs
      */
    case class LaunchVM  (id : Long, os : String, capas : Map [String, Int], user : String, script : String)
    case class PauseVM   (id : Long)
    case class ResumeVM  (id : Long)
    case class KillVM    (id : Long)
    case class VmReady   (id : Long, log : String)
    case class VmError   (id : Long, log : String)
    case class VmResumed (id : Long)
    case class VmOff     (id : Long, log : String)

    /**
      * Message pour les fichiers
      */
    case class RecvFile    (id : Long, addr : String, t_idSrc : Long, t_idDst : Long, user : String, in_file : String, out_file : String)
    case class RecvInput   (id : Long, addr : String, t_idDst : Long, user : String, in_file : String, out_file : String)
    case class RecvExe     (id : Long, e_id : String, args : String, srcPath : String, dstPath : String, addr : String)
    case class FileSent    (id : Long)
    case class FileFailure (id : Long)
    case class DownloadResult (id : Long, user : String, in_file : String, t_idSrc : Long, path : String)    

    /**
      * Message pour les tâches
      */
    case class RunTask     (id : Long, user : String, vm : Long, params : String)
    case class StopTask     (id : Long)

    case class TaskEnd     (id : Long)
    case class TaskFailure (id : Long, log : String)
    case class RmTaskDir   (id : Long, user : String)
    case class RmTaskInput (id : Long, user : String, path : String)

    case object Ack

}
