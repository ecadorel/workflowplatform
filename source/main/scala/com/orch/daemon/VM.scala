package com.orch.daemon

import org.json4s._
import java.io._
import java.net.ConnectException
import com.orch.utils._
import java.util.{Map => JMap, List => JArray}
import org.yaml.snakeyaml.Yaml
import scala.collection.JavaConverters._
import akka.actor.{ ActorRef }
import scala.collection.mutable.Queue
import com.jcraft.jsch._;
import java.util.stream._
import util.control.Breaks._

 class MyUserInfo extends UserInfo {
     def getPassword() : String = { null }
     def promptYesNo(str : String) : Boolean = { true }              
     def getPassphrase() : String = { null }
     def promptPassphrase(x: String): Boolean = { true }
     def promptPassword(x: String): Boolean = { true }
     def showMessage(x: String): Unit = {}
}

object Vagrant {
    import com.orch.db._
    import com.orch.daemon.DaemonProto._
    import com.orch.utils.{Path, Exe}
    import com.orch.file._

    var mac_of_vms : Map [Long, String] = Map ()
    var ips_of_vms : Map [Long, String] = Map ()
    var booting_vms : Map [Long, Thread] = Map ()
    val boot_conflict : Integer = new Integer (0)


    def startNfsDir () : Unit = {
        val export_line = "\"" + Global.nfs_path + "\" 192.168.122.0/24(rw,no_subtree_check,all_squash,anonuid=0,anongid=0,fsid=180077614)\n"
        Path.writeFile (Path ("/etc/exports"), export_line, true)
        val exe = new Exe (Seq ("exportfs", "-ra"), Path ("."))
        exe.synchro ()

        val exe_2 = new Exe (Seq ("service", "nfs-kernel-server", "start"), Path ("."))
        exe_2.synchro ()
    }

    def user_data (user : String, ssh_key : Path) : String = {
        val key = Path.readFile (ssh_key)

        var data : Map [String, Object] = Map ()
        data = data + ("groups" -> "wheel");
        data = data + ("lock_passwd" -> "false");
        data = data + ("name"->  "phil");
        data = data + ("shell" ->  "/bin/bash");
        data = data + ("ssh-authorized-keys" -> List (key).asJava);
        data = data + ("sudo" -> List ("ALL=(ALL) NOPASSWD:ALL").asJava);

        var users : Map [String, Object] = Map ()
        users = users + ("users" -> List (data.asJava).asJava)

        var yaml = new Yaml();
        var writer = new StringWriter();
        yaml.dump(users.asJava, writer);

        "#cloud-config\n---\n" + writer.toString ()
    }

    def meta_data (id : String) : String = {
        var data : Map [String, Object] = Map ()
        data = data + ("instance-id" -> id)
        data = data + ("local-hostname" -> id)

        var yaml = new Yaml();
        var writer = new StringWriter();
        yaml.dump(data.asJava, writer);

        writer.toString ()
    }

    def createDirAndFileOfVM (id : Long, os : String) : Unit = {
        val os_qcow2 = Path.build (Global.os_path, os + ".qcow2")
        val v_path = Path.build (Global.vm_path, s"v$id")
        val os_dst = Path.build (Path.build (Global.vm_path, s"v$id"), s"v$id.qcow2")
        println ("VM path : " + Global.vm_path + " ")
        Path.mkdirs (v_path)
        Path.copyFile (os_qcow2, os_dst)
        Path.setPermission (os_dst)

        Path.writeFile (Path.build (v_path, "user-data"), user_data ("phil", Path.build (Global.key_path, "key.pub")))
        Path.writeFile (Path.build (v_path, "meta-data"), meta_data (s"v$id"))

        val exe = new Exe (
            Seq ("mkisofs", "-o",
                Path.build (v_path, "user.iso").file,
                "-V", "cidata", "-J", "-r",
                Path.build (v_path, "user-data").file,
                Path.build (v_path, "meta-data").file),
            v_path)

        exe.synchro ()

        val prep = new Exe (
            Seq ("virt-sysprep", "-a", os_dst.file), v_path
        )

        prep.synchro ()
    }    

    def installAndLaunchVM (id : Long, capas : Map [String, Int]) : Unit = {
        val v_path = Path.build (Global.vm_path, s"v$id")
        val os_dst = Path.build (v_path, s"v$id.qcow2")
        val iso_dst = Path.build (v_path, "user.iso")
        val exe = new Exe (Seq ("virt-install",
            "--import",
            "--name", s"v$id",
            "--ram", "" + capas ("memory"),
            "--vcpus", "" + capas ("cpus"),
            "--disk", os_dst.file + ",format=qcow2,bus=virtio",
            "--disk", iso_dst.file + ",device=cdrom",
            "--network", "bridge=virbr0,model=virtio",
            "--os-type", "linux",
            "--os-variant", "ubuntu18.04",
            "--virt-type", "kvm",
            "--noautoconsole"
        ), Path ("."))

        boot_conflict.synchronized { // Visiblement on peut pas trop faire ça en parallèle
            var good = false
            while (!good) {
                val (res_, out_, err_) = exe.synchro ()
                if (res_ == 0) good = true
                println (out_, " ", err_)
            }
        }
    }

    def waitAndStoreIp (id : Long) : String = {
        import org.json4s.native.JsonMethods._

        var getMac = false
        var mac_addr = ""
        while (!getMac) {
            val exe = new Exe (Seq ("virsh", "dumpxml", s"v$id"), Path ("."))
            val (res, out, err) = exe.synchro ()
            val doc = scala.xml.XML.loadString (out)
            val mac = doc \\ "mac"
            if (mac.length != 0) {
                getMac = true
                mac_addr = mac (0).attribute ("address").getOrElse ("").toString ()
            }
        }

        this.mac_of_vms.synchronized {
            mac_of_vms = mac_of_vms + (id -> mac_addr)
        }

        println ("MAC : " + mac_addr)
        while (true) {
            val json_file = parse (Path.readFile (Path ("/var/lib/libvirt/dnsmasq/virbr0.status")))
            if (json_file.values != None) {
                val values = json_file.values.asInstanceOf[List[Map[String, String]]]
                for (vm_node <- values) {
                    if (vm_node ("mac-address") == mac_addr) {
                        this.ips_of_vms.synchronized {
                            ips_of_vms = ips_of_vms + (id -> vm_node ("ip-address"))
                        }
                        println ("IP : " + vm_node ("ip-address") + " " + id)
                        return vm_node ("ip-address")
                    }
                }
            }
            Thread.sleep (200)
        }
        ""
    }

    def executeInVM (ip : String, cmd : String) : (Int, String, String) = {
        println (s"CMD ssh : $ip : $cmd")
        val key_path = Path.build (Global.key_path, "key")

        val jsch = new JSch ();
        jsch.addIdentity(key_path.file)
        val session=jsch.getSession("phil", ip, 22);
        val ui=new MyUserInfo();
        session.setUserInfo(ui);
        session.connect();
        val channel=session.openChannel("exec");
        channel.asInstanceOf[ChannelExec].setCommand(cmd);

        val commandOutput = channel.getExtInputStream();

        val outputBuffer = new StringBuilder();
        val errorBuffer = new StringBuilder();

        val in = channel.getInputStream();
        val err = channel.getExtInputStream();

        channel.connect();

        var status = -1
        val len = 1024
        val tmp = Array.ofDim[Byte](len)

        breakable {
            while (true) {
                breakable {
                    while (in.available() > 0) {
                        val i = in.read(tmp, 0, len);
                        if (i < 0) break
                        outputBuffer.append(new String(tmp, 0, i));
                    }
                }

                breakable {
                    while (err.available() > 0) {
                        val i = err.read(tmp, 0, len);
                        if (i < 0) break
                        errorBuffer.append(new String(tmp, 0, i));
                    }
                }

                if (channel.isClosed()) {
                    if ((in.available() <= 0) && (err.available() <= 0)) {
                        status = channel.getExitStatus ()
                        System.out.println("exit-status: " + status);
                        break
                    }
                }

                try {
                    Thread.sleep(200);
                } catch {
                    case e : Exception => {}
                }
            }
        }

        System.out.println("output: " + outputBuffer.toString());
        System.out.println("error: " + errorBuffer.toString());

        channel.disconnect();        
        session.disconnect();        
        return (status, outputBuffer.toString (), errorBuffer.toString ())
    }


    def mountNfsSharedFolder (id : Long, ip : String, user : String, ref : ActorRef) : Unit = {
        val path_user_vm = Path.build ("/home/phil/", user)        
        Path.mkdirs (Path.build (Global.nfs_path, user))


        val cmd = "mkdir " + path_user_vm.file + "; sudo mount 192.168.122.1:/tmp/" + user + " " + path_user_vm.file;

        while (true) {
            try {
                val (status, out, err) = executeInVM (ip, cmd)
                return
            } catch {
                case e : Throwable => {
                    println (e)
                    Thread.sleep (200)
                }
            }
        }
    }


    def launchVM (id : Long, os : String, capas : Map [String, Int], user : String, script : String, ref : ActorRef) : Unit = {
        val thread = new Thread () {
            override def run () : Unit = {
                try {
                    createDirAndFileOfVM (id, os)
                    installAndLaunchVM (id, capas)
                    val ip = waitAndStoreIp (id)
                    mountNfsSharedFolder (id, ip, user, ref)
                    ref ! DaemonProto.VmReady (id, "")
                } catch {
                    case _ : InterruptedException => {}
                }
            }
        }

        this.booting_vms.synchronized {
            booting_vms = booting_vms + (id -> thread)            
        }

        thread.start ()
    }

    def removeBooting (id : Long) : Unit = {
        this.booting_vms.synchronized {
            this.booting_vms = this.booting_vms.filterKeys (_ != id)
        }
    }    

    def runTask (id : Long, user : String, v_id : Long, params : String, ref : ActorRef) : Unit = {
        val ip = this.ips_of_vms.synchronized {
            if (this.ips_of_vms.contains (v_id))
                this.ips_of_vms (v_id)
            else {
                println (s"No vm : $v_id ${ips_of_vms}")
                ref ! DaemonProto.TaskFailure (id, "")
                return 
            }
        }

        val e_path = Path.build (Path.build (Global.user_home, user), ""+ id)
        val path_in_vm = Path.build (Path.build ("/home/phil", user), "" + id)

        val innerCommand = "cd " + path_in_vm.file + " && ./launch.sh " + params;

        val thread = new Thread () {
            override def run () : Unit = {
                val untar = new Exe (Seq ("tar", "xf", "exec.tar"), e_path)
                untar.synchro ()

                while (true) {
                    try {

                        val (status, out, err) = executeInVM (ip, innerCommand)
                        println (id + " " + status + " " + out + " " + err)
                        if (status != 0) {
                            ref ! DaemonProto.TaskFailure (id, out)
                        } else ref ! DaemonProto.TaskEnd (id)
                        return
                    } catch {
                        case e: Throwable => {
                            println (e)
                            Thread.sleep (200)
                        }
                    }
                }
            }
        }



        thread.start ()
    }

    def killVM (id : Long, ref : ActorRef) {
        val thread = new Thread () {
            override def run () {
                booting_vms.synchronized {
                    if (booting_vms.contains (id)) {
                        println (s"INTERRUPT : ${id}")
                        booting_vms (id).interrupt ()
                        removeBooting (id)
                    }
                }

                val destroy = new Exe (Seq ("virsh", "destroy", s"v$id"), Path ("."))
                val undefine = new Exe (Seq ("virsh", "undefine", s"v$id"), Path ("."))

                destroy.synchro ()
                undefine.synchro ()

                ref ! DaemonProto.VmOff (id, "")

                var mac = ""

                mac_of_vms.synchronized {
                    if (mac_of_vms.contains (id))
                        mac = mac_of_vms (id)
                    mac_of_vms = mac_of_vms.filterKeys (_ != id)
                }

                deleteRecursively (new java.io.File (Path.build (Global.vm_path, s"v$id").file))

                var ip = ""                
                ips_of_vms.synchronized {
                    if (ips_of_vms.contains (id))
                        ip = ips_of_vms (id)
                    ips_of_vms = ips_of_vms.filterKeys (_ != id)
                }

                if (ip != "" && mac != "") {
                    val lease = new Exe (Seq ("dhcp_release", "virbr0", ip, mac), Path ("."))
                    lease.synchro ()
                }
            }
        }

        thread.run ()
    }

    def deleteRecursively(file: java.io.File): Unit = {
        if (file.isDirectory)
            file.listFiles.foreach(deleteRecursively)

        if (file.exists)
            file.delete
    }

}
