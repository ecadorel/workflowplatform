package com.orch.shell

import org.jline
import org.jline.reader._
import org.jline.terminal._
import akka.actor.{Props, Actor, ActorSystem, ActorRef, ActorLogging, ActorSelection, PoisonPill }
import com.typesafe.config.ConfigFactory
import scopt.OParser
import scala.io.Source
import scala.sys.process._
import scala.math.sqrt


case class SecondTask (t_id : Long, v_id : Long, e_id : String, user : String, state : Int, start : Long, end : Long, len : Long)

case class ExecDev (e_id : String, mean : Long, variance : Long, dev : Double, certainty : List [Long])


case class SecondVM (v_id : Long, n_id : String, f_id : Long, user : String, state : Int, start : Long, ready : Long, end : Long, len : Long)


class Monitor (LAddr : String, LPort : Int, RAddr : String, RPort : Int) extends Actor with ActorLogging {


    import com.orch.master.MonitorProto._
    import com.orch.db._
    import com.orch.file._
    import com.orch.utils._

    var master: ActorSelection = null
    var coll = new Collection ("orch")

    def uniform (low : Int, high : Int) : Int = {
        val x = scala.util.Random.nextInt % (high - low)
        if (x < 0) -x + low
        else x + low
    }

    override def preStart () : Unit = {
        println ("Starting monitor")
        println (s"Register to master node at $RAddr:$RPort")
        master = context.actorSelection(s"akka.tcp://RemoteSystem@$RAddr:$RPort/user/master")
        master ! MonitorProto.AddMonitor (LAddr, LPort)
    }

    def receive : Receive = {
        /**
          * Shell
          */
        case command : Command =>
            command.context match {
                case "ls" => lsCommand (command)
                case "node" => nodeCommand (command)
                case "vm" => vmCommand (command)
                case "flavor" => flavorCommand (command)
                case "exec" => execCommand (command)
                case "task" => taskCommand (command)
                case "file" => fileCommand (command)
                case "repl" => replicaCommand (command)
                case "cluster" => clusterCommand (command)
                case "" => {}
                case _ => println (s"Command not found '" + command.context + "'")
            }

        /**
          * Daemon
          */
        case MonitorProto.NoDaemon (id) => println (s"No daemon $id")

        /**
          * VM
          */
        case MonitorProto.VmReady  (id, log) => println (s"VM $id is ready : \n$log")
        case MonitorProto.VmError  (id, log) => println (s"VM $id is error : \n$log")
        case MonitorProto.VmOff    (id, log) => println (s"VM $id is down : \n$log")
        case MonitorProto.NoVm     (id)      => println (s"No VM $id")
        case MonitorProto.VmIdUsed (id)      => println (s"Vm Id $id is already used")

        /**
          * Flavor
          */
        case MonitorProto.NoFlavor   (id) => println (s"No flavor $id")
        case MonitorProto.FlavIdUsed (id) => println (s"Flavor Id $id is already used")

        /**
          *  Execs
          */
        case MonitorProto.NoTask      (id) => println (s"No task $id")
        case MonitorProto.TaskEnd     (id) => println (s"Task $id is finished")
        case MonitorProto.TaskFailure (id, log) => println (s"Task $id has failed : \n$log")
        /**
          * Replica and files
          */
        case MonitorProto.NoReplica    (id) => println (s"No replica $id")
        case MonitorProto.NoFile       (id) => println (s"No file $id")
        case MonitorProto.FileSent     (id) => println (s"File $id is sent")
        case MonitorProto.FileFailure  (id) => println (s"File $id failed")

        /**
          * Exec upload 
          */
        case FileProto.FileSent (id) => println (s"executable $id uploaded")
        case FileProto.FileFailure (id) => println (s"executable $id failed to upload")
    }

    def lsCommand (command : Command) : Unit = {
        val contents = Process("ls " + command.mode).lineStream
        for (z <- contents)
            println (z);
    }

    def nodeCommand (command: Command) : Unit = {
        command.mode match {
            case "load" =>
                try {
                    val filename = command.args (0)
                    val list = Loader.parseNodeFile (filename)
                    for (node <- list) {
                        master ! MonitorProto.AddDaemon (node.addr, node.port, node.cluster, node.n_id, node.speed, node.capas)
                    }
                    val cluss = Loader.parseClusterFile (filename)
                    for (cluster <- cluss) {
                        master ! MonitorProto.AddCluster (cluster._1, cluster._2)
                    }
                } catch {
                    case e : Throwable => {
                        println (e)
                        println ("node load <path:string> : load list of nodes from a file")
                    }
                }
            case "list" =>
                val nodes = this.coll.findNodes ()
                println (Formatter.createTable[Node] (nodes))
            case _ =>
                println ("command not found " + command.mode + " for node")
                println ("node : ")
                println ("\t load <path:string< : load list of nodes from a file")
                println ("\t list : print the list of compute nodes")
        }
    }

    def clusterCommand (command : Command) : Unit = {
        command.mode match {
            case "add" =>
                try {
                    val name = command.args (0)
                    var bw : Map [String, Int] = Map ()
                    for (i <- 1 until command.args.length) {
                        val elems = command.args (i).split (":")
                        val cluster = elems (0)
                        val size = elems (1).toInt
                        bw += (cluster -> size)
                    }
                    master ! MonitorProto.AddCluster (name, bw)
                } catch {
                    case e : Throwable => {
                        println ("cluster add <name:string> [<cluster:string>:<bw:int>]* : add a new cluster with bandwidth informations")
                    }
                }
            case "list" =>
                val clusters = this.coll.findClusters ()
                println (Formatter.createTable [Cluster] (clusters))
            case _ =>
                println ("command not found " + command.mode + " for cluster")
                println ("cluster : ")
                println ("\t add <name:string> [<cluster:string>:<bw:int>]* : add a new cluster with bandwidth informations")
                println ("\t list : print the list of clusters")
        }
    }

    def vmCommand (command: Command) : Unit = {
        command.mode match {
            case "boot" =>
                try {
                    master ! MonitorProto.LaunchVM (command.args (0).toInt, command.args (1).toInt, command.args (2), command.args (3))
                } catch {
                    case e : Throwable =>
                        println ("vm boot <id:int> <flavor:int> <user:string> <node:string> : boot a vm")
                }
            case "kill" =>
                try {
                    master ! MonitorProto.KillVM (command.args (0).toLong)
                } catch {
                    case e : Throwable =>
                        println ("vm kill <id:int> : kill a vm")
                }

            case "del" =>
                try {
                    master ! MonitorProto.DelVM (command.args (0).toLong)
                } catch {
                    case e : Throwable =>
                        println ("vm del <id:int> : remove a vm from the list of known VM (kill it first)")
                }
            case "list" =>
                val vms = this.coll.findVMs ()
                var list : List [SecondVM] = List ()
                for (v <- vms)
                    list = list :+ SecondVM (v.v_id, v.n_id, v.f_id, v.user, v.state, v.start, v.ready, v.end, v.ready - v.start)
                println (Formatter.createTable[SecondVM] (list))

            case "dev" =>
                val vms = this.coll.findVMs ()
                val flavors = this.coll.findFlavors ()
                var map : Map [String, (Long, List [Long])] = Map ()
                for (v <- vms) {
                    val os = flavorOs (v.f_id, flavors)
                    if (map.contains (os)) {
                        map += (os -> (map (os)._1 + (v.ready - v.start), map (os)._2 :+ (v.ready - v.start)))
                    } else {
                        map += (os -> ((v.ready - v.start), List (v.ready - v.start)))
                    }
                }

                var list : List [ExecDev] = List ()
                for (exec <- map) {
                    if (exec._2._2.length != 0) {
                        val mean = exec._2._1 / exec._2._2.length
                        var variance : Long = 0
                        for (x <- exec._2._2)
                            variance = variance + ((x - mean) * (x - mean))

                        if (exec._2._2.length > 1) {
                            variance /= (exec._2._2.length - 1)
                            val sd = sqrt (variance)
                            list = list :+ ExecDev (exec._1, mean, variance, sd,
                                (for (i <- 10 to 101 if i % 10 == 0) yield (
                                    NormalDistribution (mean.toInt, sd.toInt).inverseCDF (i/100.0).toLong
                                )).toList
                            )                            
                        } else {
                            list = list :+ ExecDev (exec._1, mean, 0, 0, List ())
                        }
                    }
                }
                println (Formatter.createTable [ExecDev] (list))

            case _ =>
                println ("command not found " + command.mode + " for vm")
                println ("vm : ")
                println ("\t boot <id:int> <user:string> <flavor:int> <node:string> : boot a new vm")
                println ("\t kill <id:int> : kill a running vm")
                println ("\t del <id:int> : remove a vm from the list of known VM (kill it first)")
                println ("\t list : print the list of vms")
        }
    }

    def flavorCommand (command: Command) : Unit = {
        command.mode match {
            case "add" =>
                try {
                    var capas : Map [String, Int] = Map ()
                    capas += ("cpus" -> command.args (2).toInt, "memory" -> command.args (3).toInt)
                    master ! MonitorProto.AddFlavor (command.args (0).toInt, command.args (1), capas, "")
                } catch {
                    case e : Throwable =>
                        println ("flavor add <id:int> <os:string> <cpu:int> <mem:int> : add a new flavor")
                }
            case "list" =>
                val flavors = this.coll.findFlavors ()
                println (Formatter.createTable[Flavor] (flavors))
            case _ =>
                println ("command not found " + command.mode + " for flavor")
                println ("vm : ")
                println ("\t add <id:int> <os:string> <cpu:int> <mem:int> : add a new flavor")
                println ("\t list : print the list of flavors")
        }
    }

    def execCommand (command: Command) : Unit = {
        command.mode match {
            case "upl" =>
                try {
                    import java.net.InetSocketAddress

                    var id = uniform (0, 100)
                    val src_path = command.args (1)
                    val e_id = command.args (0)
                    val e_path = new java.io.File (src_path).getAbsolutePath ()

                    println (s"Uploading exec : $id")
                    master ! MonitorProto.UploadExec (e_id, e_path)
                } catch {
                    case e : Throwable =>
                        println ("exec upl <id:string> <path:string> : upload an executable")
                }
            case "run" =>
                try {
                    master ! MonitorProto.RunTask (command.args (0).toInt, command.args (1), command.args (2).toInt, "")
                } catch {
                    case e : Throwable =>
                        println ("exec run <id:int> <exec:string> <vm:int> : run an executable on a VM")
                }
            case "list" =>
                var execs = this.coll.findExecutables ()
                println (Formatter.createTable[Executable] (execs))
            case _ =>
                println ("command not found " + command.mode + " for executable")
                println ("exec : ")
                println ("\t upl <id:string> <path:string> : upload an executable")
                println ("\t run <id:int> <exec:string> <vm:int> : run an executable on a vm")
                println ("\t list : print the list of executable")
        }
    }

    def taskCommand (command: Command) : Unit = {
        command.mode match {
            case "list" =>
                val tasks = this.coll.findTasks ()
                var list : List[SecondTask] = List ()
                for (t <- tasks)
                    list = list :+ SecondTask (t.t_id, t.v_id, t.e_id, t.user, t.state, t.start, t.end, t.end - t.start)
                println (Formatter.createTable[SecondTask] (list))
            case "running" =>
                val tasks = this.coll.findTasksRunning ()
                println (Formatter.createTable[Task] (tasks))
            case "dev" =>
                val tasks = this.coll.findTasks ()
                // e_id => Sum, x
                var map : Map [String, (Long, List [Long])] = Map ()
                for (t <- tasks) {
                    if (map.contains (t.e_id)) {
                        map += (t.e_id -> (map (t.e_id)._1 + (t.end - t.start), map (t.e_id)._2 :+ (t.end - t.start)))
                    } else {
                        map += (t.e_id -> ((t.end - t.start), List (t.end - t.start)))
                    }
                }

                var list : List [ExecDev] = List ()
                for (exec <- map) {
                    if (exec._2._2.length != 0) {
                        val mean = exec._2._1 / exec._2._2.length
                        var variance : Long = 0
                        for (x <- exec._2._2) {
                            variance = variance + ((x - mean) * (x - mean))
                        }
                        if (exec._2._2.length > 1) {
                            variance /= (exec._2._2.length) - 1
                            val sd = sqrt (variance)
                            list = list :+ ExecDev (exec._1, mean, variance, sd,
                                (for (i <- 10 to 101 if i % 10 == 0) yield (
                                    NormalDistribution (mean.toInt, sd.toInt).inverseCDF (i/100.0).toLong
                                )).toList
                            )
                        } else {
                            list = list :+ ExecDev (exec._1, mean, 0, 0, List ())
                        }
                    }
                }

                println (Formatter.createTable [ExecDev] (list))
            case _ =>
                println ("command not found " + command.mode + " for task")
                println ("task : ")
                println ("\t list : print the list of task")
        }
    }

    def fileCommand (command: Command) : Unit = {
        command.mode match {
            case "reg" =>
                try {
                    master ! MonitorProto.AddFile (command.args (0).toInt, command.args (1), command.args (2), command.args (3).toInt)
                } catch {
                    case e : Throwable =>
                        println ("file reg <id:int> <filename:string> <user:string> <size:int> : register a new file")
                }

            case "input" =>
                try {
                    val src_path = command.args (1)
                    val abs = new java.io.File (src_path).getAbsolutePath ()
                    master ! MonitorProto.RegisterInput (command.args (0).toInt, abs)
                } catch {
                    case e : Throwable =>
                        println ("file input <id:int> <path:string> : register an input file")
                }
            case "send" =>
                try {
                    master ! MonitorProto.SendInput (command.args (0).toInt, command.args (1).toInt, command.args (2), command.args (3).toInt)
                } catch {
                    case e : Throwable =>
                        println ("file send <f_id:int> <r_idDst:int> <n_dst:string> <t_dst:int> : send an input file")
                }

            case "list" =>
                val files = this.coll.findFiles ()
                println (Formatter.createTable[File] (files))
            case "io" =>
                val files = this.coll.findIOs ()
                println (Formatter.createTable[IOFile] (files))

            case _ =>
                println ("command not found " + command.mode + " for file")
                println ("file : ")
                println ("\t reg <id:int> <filename:string> <user:string> <size:int> : register a new file")
                println ("\t list : print the list of files")
        }
    }

    def replicaCommand (command: Command) : Unit = {
        command.mode match {
            case "reg" =>
                try {
                    master ! MonitorProto.ReplicaReg (command.args (0).toInt, command.args (1).toInt, command.args (2), command.args (3).toInt)
                } catch {
                    case e : Throwable =>
                        println ("repl reg <id:int> <f_id:int> <node:string> <t_id:int> : register a new replica for file f_id produce by task t_id on node")
                }
            case "send" =>
                try {
                    master ! MonitorProto.SendFile (command.args (1).toInt, command.args (0).toInt, command.args (2), command.args (3).toInt)
                } catch {
                    case e : Throwable =>
                        println ("repl send <id:int> <r_id:int> <n_dst:string> <t_dst:int> : create a replica from r_id replica on node n_id for task t_id")
                }
            case "sending" =>
                try {
                    val repls = this.coll.findReplicasSending ()
                    println (Formatter.createTable[Replica] (repls))
                } catch {
                    case _ : Throwable => println ("repl list <id:int> : list the replicas of a given file")
                }

            case "list" =>
                try {
                    val repls = this.coll.findReplicas ()
                    println (Formatter.createTable[Replica] (repls))
                } catch {
                    case _ : Throwable => println ("repl list <id:int> : list the replicas of a given file")
                }
            case _ =>
                println ("command not found " + command.mode + " for replica")
                println ("repl : ")
                println ("\t send <id:int> <r_id:int> <n_dst:string> <t_dst:int> : create a replica from r_id replica on node n_id for task t_id")
                println ("\t reg <id:int> <f_id:int> <t_id:int> : register a new replica for file f_id produce by task t_id")
                println ("\t list <id:int> : print the list of files of a given file")
        }

    }

    def flavorOs (f_id : Long, flavors : Seq [Flavor]) : String = {
        for (f <- flavors)
            if (f.f_id == f_id) return f.os
        return ""
    }


    override def postStop () : Unit = {
        println ("Stopping monitor")
        master ! MonitorProto.RmMonitor (LAddr, LPort)
    }

}


object Shell {


    def props (local_addr : String, local_port : Int, remote_addr : String, remote_port : Int) : Props =
        Props (new Monitor (local_addr, local_port, remote_addr, remote_port))

    def configFile (addr: String, port: Int):String = {
        s"""akka {
      log-dead-letters-during-shutdown=off
      log-dead-letters=off
      loglevel = "INFO"
      actor {
        warn-about-java-serializer-usage=off
        provider = "akka.remote.RemoteActorRefProvider"
      }
      remote {
        log-remote-lifecycle-events=off
        enabled-transports = ["akka.remote.netty.tcp"]
        netty.tcp {
          hostname = $addr
          port = $port
        }
        log-sent-messages = on
        log-received-messages = on
      }
    }"""
    }

    def parseOptions (args : Array[String]) : Options.Config = {
        OParser.parse (Options.parser1, args, Options.Config()).getOrElse (Options.Config ())
    }

    def runCommand (actor : ActorRef, command : String) : Unit = {
        actor ! CommandParser.parse (command)
    }

    def createSystem (local_addr : String, local_port : Int) : ActorSystem = {
        val config = ConfigFactory.parseString (configFile (local_addr, local_port))
        ActorSystem ("RemoteSystem", config)
    }

    def launchActor (system : ActorSystem, config : Options.Config) : ActorRef = {
        system.actorOf (
            Shell.props (config.local_addr, config.local_port, config.remote_addr, config.remote_port), name="monitor"
        )
    }

    def main (args : Array[String]) {
        val config = parseOptions (args)
        val system = createSystem (config.local_addr, config.local_port)
        val act = launchActor (system, config)
        val terminal = TerminalBuilder.builder ().build ()
        val line = LineReaderBuilder.builder ().terminal (terminal).build ()

        var endL = false
        while (!endL) {
            try {
                val st = line.readLine ("> ")
                runCommand (act, st)
            } catch {
                case user : UserInterruptException =>
                case end : EndOfFileException =>
                    println ("Bye")
                    endL = true
            }
        }

        terminal.close ();

        act ! PoisonPill
        system.terminate ()
    }

}
