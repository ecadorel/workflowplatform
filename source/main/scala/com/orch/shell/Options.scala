package com.orch.shell;
import scopt.OParser

object Options {

    case class Config(
        local_addr : String = "127.0.0.1",
        local_port : Int = 5400,
        remote_addr : String = "127.0.0.1",
        remote_port : Int = 5100
    )


    val builder = OParser.builder[Config]

    val parser1 = {
        import builder._
        OParser.sequence(
            programName("shell"),
            head("scopt", "4.x"),
            // option -f, --foo
            opt[String]('r', "remote-addr")
                .action((x, c) => c.copy(remote_addr = x))
                .text("remote address of the master node"),

            opt[String]('l', "local-addr")
                .action((x, c) => c.copy (local_addr = x))
                .text ("local address, used to remote access to this local node"),

            opt[Int]('p', "local-port")
                .action ((x, c) => c.copy (local_port = x))
                .text ("local port, used to remote access to this local node"),

            opt[Int]("remote-port")
                .action ((x, c) => c.copy (remote_port = x))
                .text ("remote port, of the master node")
        )
    }

}
