package com.orch.shell

case class Command (
  context : String = "",
  mode : String = "",
  args : Array[String] = Array ()
)

object CommandParser {

  def parse (command : String) : Command = {
    val content = command.split (' ').map {x => x.trim}
    if (content.length == 0) {
      Command ("", "", Array ())
    } else {
      if (content.length < 2)
        Command (content(0), "", Array ())
      else Command (content (0), content (1), content.slice (2, content.length))
    }
  }

}
