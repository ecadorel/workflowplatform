package com.orch.utils

/**
  * Calcul d'une distribution normal
  */
case class NormalDistribution (mu : Int, sigma : Int) {

    def PDF (x : Double) : Double = probabilityDensityFunction (x)

    def probabilityDensityFunction (x : Double) : Double = {
        innerPDF ((x - mu) / sigma) / sigma
    }

    def CDF (x : Double) : Double = cumulativeDistributionFunction (x)

    def cumulativeDistributionFunction (x : Double) : Double = {
        innerCDF ((x - mu) / sigma)
    }

    def inverseCDF (x : Double) : Double = inverseCumulativeDistributionFunction (x)

    def inverseCumulativeDistributionFunction (x : Double) : Double = {
        inverseCDFSearch (
            x,
            (-8.0 * sigma) + mu,
            (8.0 * sigma) + mu,
            0.000001 // Pas de Double.espilon en scala !!
        )
    }

    private def innerCDF (x : Double) : Double = {
        if (x < -8.0) 0.0
        else if (x > 8.0) 1.0
        else {
            var sum = 0.0
            var term = x
            var i = 3
            while (sum + term != sum) {
                sum = sum + term
                term = (term * x * x) / i
                i += 2
            }
            0.5 + sum * innerPDF (x)
        }
    }

    private def innerPDF (x : Double) : Double = {
        import scala.math.{exp, sqrt, Pi}
        exp (- (x * x) / 2.0 ) / sqrt (2.0 * Pi)
    }

    private def inverseCDFSearch (x : Double, lo : Double, hi : Double, lastMid : Double) : Double = {
        val eps = 0.000001
        val mid = lo + (hi - lo) / 2.0
        if (hi - lo <= eps) mid
        else if (hi - lo == lastMid) mid
        else {
            if (CDF (mid) > x) inverseCDFSearch (x, lo, mid, hi - lo)
            else inverseCDFSearch (x, mid, hi, hi - lo)
        }
    }

}
