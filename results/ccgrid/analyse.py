import os
import zipfile
import subprocess
from os import walk
from pymongo import MongoClient
import pymongo
import requests
import json
import matplotlib.pyplot as plt
import numpy as np
import yaml
import argparse

global_report = {}

# =============================================================================================================
# ===                                           LOADING                                                    ====
# =============================================================================================================

# Loading of the zip files in the current directory 
# Each file is loaded in the mongodb tmp_base
def getAllZips (current_path) : 
    f = []
    for (dirpath, dirnames, filenames) in walk(current_path):
        for j in filenames :
            if (len (j) >= 4 and j [-4:] == ".zip") :
                f = f + [j]
        break
    return f

# Load a mongodb file into database, the database loaded is named tmp_base
def loadInDatabase (path) :
    with zipfile.ZipFile(path, 'r') as zip_ref:
        zip_ref.extractall(".")

    client = MongoClient ()
    client.drop_database ("tmp_base")
    cmd = "mongorestore backup/orch --drop -d tmp_base"
    print subprocess.check_output(cmd,stderr=subprocess.STDOUT,shell=True)

# =============================================================================================================
# ===                                           DATAS                                                      ====
# =============================================================================================================
    
# Generate a chart of the energy
# CPU load
# And the arrival of the different users
def generateGraphs (name) : 
    generateEnergy (name)
    generateCpuUsage (name)
    generateUserLoad (name)

def generateEnergy (name) :
    client = MongoClient ()
    db = client.tmp_base
    min_time = db.sched_info.find_one (sort=[("time", -1)])["time"] # l'instant ou le sched se connecte a l'orch
    max_task = db.tasks.find_one (sort=[("end", -1)])
    mongo_nodes = db.nodes.find ()
    nodes = []
    for n in mongo_nodes : # The list mongo_node is link to the mongodb, thus the copy is required 
        nodes = nodes + [n]

    makespan = max_task["end"]/1000 - min_time/1000 # total execution time
    print (makespan)
    
    # Load all the sensors of the api
    req_sensors = requests.get ("https://api.seduce.fr/sensors", verify=False) 
    json_sensors = json.loads (req_sensors.text)
    used_sensors = []
    for sens in json_sensors ["sensors"] : 
        for node in nodes:
            if (len (sens) >= len (node["n_id"]) + 1):
                if (sens [0:len (node["n_id"]) + 1] == node["n_id"]+"_") :
                    used_sensors = used_sensors + [sens[:]]

    conso_result = {}
    conso_global = []


    # Load all the data acquired by the sensors on the platform seduce during the generation of the traces
    max_time = max_task ["end"]
    for sensor in used_sensors : 
        req_result = requests.get("https://api.seduce.fr/sensors/" + sensor + "/measurements?start_date=" + str (int(min_time/1000)) + "&end_date=" + str (int(max_time/1000)), verify=False)
        json_result = json.loads (req_result.text)
        node_name = sensor [0:sensor.find ("_")]
        if node_name in conso_result : 
            conso_result [node_name] = [x + y for x, y in zip (conso_result[node_name], json_result ["values"])]
        else : 
            conso_result [node_name] = json_result ["values"]
            
        if (conso_global == []): 
            conso_global = json_result ["values"]
        else :
            conso_global = [x + y for x, y in zip (json_result ["values"], conso_global)]

    IDLE = 75
    MAX_MAKESPAN = 650
    idle_conso = (MAX_MAKESPAN - makespan) * len (nodes) * IDLE
        
    if ("conso_global" in global_report) : 
        global_report ["conso_global"][name] = sum (conso_global)
        global_report ["conso_widle"][name]  = sum (conso_global) + idle_conso
        global_report ["idle"][name]  = idle_conso
        global_report ["makespan"][name] = makespan
    else :
        global_report ["conso_global"] = {name : sum(conso_global)}
        global_report ["conso_widle"]  = {name : sum(conso_global) + idle_conso}
        global_report ["idle"]  = {name : idle_conso}
        global_report ["makespan"] = {name: makespan}

    # Generation of the charts
    plt.figure (1)
    plt.plot (conso_global, linewidth=0.5, label=name[:-4])

def generateCpuUsage (name) :
    client = MongoClient ()
    db = client.tmp_base
    max_task = db.tasks.find_one (sort=[("end", -1)])
    min_task = db.tasks.find_one (sort=[("start", 1)])
    tasks = db.tasks.find ()
    
    makespan = max_task["end"]/1000 - min_task["start"]/1000
    load = [0 for i in xrange (0, makespan)]
    for x in tasks :
        for j in xrange (x["start"]/1000, x["end"]/1000) :
            i = j - min_task ["start"]/1000
            load [i] = load [i] + 1
    plt.figure (2)
    plt.plot (load, linewidth=0.5, label=name[:-4])

def generateUserLoad (name) :
    client = MongoClient ()
    db = client.tmp_base
    min_time = db.sched_info.find_one (sort=[("time", -1)])["time"] # The begin time is the instant of the first connection of the scheduler to the master module
    max_task = db.tasks.find_one (sort=[("end", -1)])
    
    makespan = max_task["end"]/1000 - min_time/1000 # Total execution time
    print (makespan)
    if (makespan > 0) : 
        load = [0 for i in xrange (0, makespan)]
        with open ("backup/workload.yaml", 'rb') as stream : 
            try : 
                load2 = yaml.load (stream) 
                for flow in load2 ['workload'] :
                    load [int (flow ['start'])] = load [int (flow['start'])] + 1
            except yaml.YAMLError as exc:
                print(exc)        
        plt.figure (3)
        plt.plot (load, label=name[:-4])



        
# =============================================================================================================
# ===                                             ANALYSE                                                  ====
# =============================================================================================================

# The consomation analyse is performed during the generation of the energy chart
# This will analyse the number of users, and the different metrics of user fairness
def analyse (name) :
    client = MongoClient ()
    db = client.tmp_base
    users = db.tasks.distinct ("user")
    min_time = db.sched_info.find_one (sort=[("time", -1)])["time"] 
    deadlines = {}
    with open ("backup/workload.yaml", 'rb') as stream : 
        try : 
            load = yaml.load (stream) 
            for flow in load ['workload'] : 
                deadlines [flow ['user']] = int (flow ['start']) + int(flow ['deadline'])
        except yaml.YAMLError as exc:
            print(exc)        

    print (deadlines)
    max_end_users = {}
    nb_content = 0
    nb_pas_content = 0
    diff_content = 0
    diff_pas_content = 0
    for user in users :
        max_task = db.tasks.find ({"user" : user}).sort("end", pymongo.DESCENDING)[0]
        max_end_users[user] = int ((max_task ["end"] - min_time)/1000)
        if (deadlines [user] > max_end_users [user]) :
            nb_content = nb_content + 1
            diff_content = diff_content + (deadlines [user] - max_end_users [user])
        else :
            nb_pas_content = nb_pas_content + 1
            diff_pas_content = diff_pas_content + (deadlines [user] - max_end_users [user])

    nb_vm = db.vms.find ().count ()
    print (max_end_users)
    if ("diff_content" in global_report) : 
        global_report ["diff_content"][name] = diff_content
        global_report ["diff_pas_content"][name] = diff_pas_content
        global_report ["nb_content"][name] = nb_content
        global_report ["nb_vm"][name] = nb_vm
        if (nb_pas_content != 0) :
            global_report ["mean_pas_content"][name] = (diff_pas_content / (nb_pas_content * 1.0))
        else :
            global_report ["mean_pas_content"][name] = 0
    else :
        global_report ["diff_content"] = {name : diff_content}
        global_report ["diff_pas_content"] = {name : diff_pas_content}
        global_report ["nb_vm"]= {name : nb_vm}
        if nb_pas_content != 0 : 
            global_report ["mean_pas_content"] = {name: diff_pas_content / (nb_pas_content * 1.0)}
        else :
            global_report ["mean_pas_content"] = {name: 0}
        global_report ["nb_content"] = {name : nb_content}


# Generate the global report and print it to the consol
def exportGlobalReport () :
    lines = {}
    head = ""
    for k in global_report :
        values = global_report [k]
        head = head + k.center (20) + "|"
        for j in values :
            if (j in lines) :
                lines [j] = lines [j] + str (values [j]).center (20) + '|'
            else :
                lines [j] = str (values [j]).center (20) + '|'
    sret = ("").center (30) + '|' + ("").center (len (head), '#') + '\n' + ("").center (30) + '|' + head + "\n"
    for l in lines :        
        sret = sret + l.center (30) + '|' + lines [l] + '\n'
    return sret
    

                
def main (path) :
    plt.figure (1)
    plt.clf ()
    plt.figure (2)
    plt.clf ()
    plt.figure (3)
    plt.clf ()
    plt.figure (4)
    plt.clf ()
    zips = getAllZips (path)
    for zip in zips :
        print ("Generate : " + zip)
        loadInDatabase (os.path.join (path, zip))
        generateGraphs (os.path.join (path, zip))
        analyse (os.path.join (path, zip))

    plt.figure (1)
    plt.legend ()
    plt.savefig ("energy.png")    
    plt.figure (2)
    plt.legend ()
    plt.savefig ("load.png")

    plt.figure (3)
    plt.legend ()
    plt.savefig ("users.png")
    print (exportGlobalReport ())
    
if __name__ == "__main__" :
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", help="input directory", default=".")
    args = parser.parse_args()
    main (args.input)
