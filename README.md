# Workflow_platform

## Results 

The results are stored in the directory results. 
Each result is a zip file containing the traces of the execution on the platform seduce. 

Each traces is a dump of a mongodb database, that can be restored to get the results.
The python program `analyse.py` perform the analyse and generate printable results.

Its usage is : 
```
python analyse.py --input A/HEFT
```


## Source

The source directory contains the source code that has been used to execute the workflows. 
The workflows used has been generated using the [Montage API](http://montage.ipac.caltech.edu/)

The file structure is as follows : 
- com.orch.daemon : stores all the files that are usefull for the Worker module, and are executed on each compute node.
- com.orch.db : connexion to the database and storage of the log
- com.orch.file : file input/ouput used to send file between the nodes
- com.orch.master : The files used for the master module
- com.orch.scheduler : The files used for the scheduler module, where the different algorithms are the following: 
  - HEFT_deadline (or OnlyUsedNodes)
  - HEFT
  - NearDeadline
	
	In this module the selection of the algorithm is done in the ScheduleActor file
	
- com.orch.shell : a tool to monitor the master node
- com.orch.utils : common utils for every modules


## Execo

The directory execo contains an example to deploy an execution on the grid5000 platform.
- `main.py` is the script to execute
- `scenario.yaml` is an example of possible scenario
- `toy/` is an example of possible workflow application, with two tasks, the number used for file size, execution time, and capacity needs are fake


